import { Routes } from '@angular/router';
import { LoginComponent } from './login/login/login.component';

import { ProspectQuestionaireComponent } from './prospects/prospect-questionaire/prospect-questionaire.component';
import { StyleguideComponent } from './styleguide/styleguide.component';
import { WhitePapersComponent } from './white-papers/white-papers.component';

import { SitemapComponent } from './sitemap/sitemap.component';
import { ErrorComponent } from './errors/error/error.component';
import { RouteManager } from './routeManager';

export const appRoutes: Routes = [
{
	path: '',
	component: LoginComponent
},
{
	path: 'login',
	component: LoginComponent
},
{
	path: 'prospects',
	loadChildren: './prospects/prospect.module#ProspectModule',
	canActivate: [RouteManager], data: { roles: ['SalesAssociate'] }
},
{
	path: 'whitepapers',
	component: WhitePapersComponent,
	canActivate: [RouteManager], data: { roles: ['SalesAssociate'] }
},
{
	path: 'styleguide',
	component: StyleguideComponent,
	canActivate: [RouteManager], data: { roles: ['SalesAssociate'] }
},
{
	path: 'sitemap',
	component: SitemapComponent,
	canActivate: [RouteManager], data: { roles: ['SalesAssociate'] }
},
{
	path: '**',
	component: ErrorComponent //use to keep wrong link in url
	//redirectTo: '404' //redirect to /404
}  
	];