import { Component, OnInit } from '@angular/core';
import { fadeInTransition } from '../_animations/animations';

@Component({
	selector: 'app-styleguide',
	templateUrl: './styleguide.component.html',
	styleUrls: ['./styleguide.component.scss'],
	animations: [fadeInTransition],
	host: { '[@fadeInTransition]': '' }
})
export class StyleguideComponent implements OnInit {

	public states= ['NY', 'NJ', 'TX'];
	constructor() { }

	ngOnInit() {
	}

}
