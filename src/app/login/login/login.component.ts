import { Component, OnInit } from '@angular/core';
import { fadeInTransition, fadeInOut } from '../../_animations/animations';
import { NgForm } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../_services/authentication.service';
import { TokenService } from '../../_services/token.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	animations: [fadeInTransition, fadeInOut],
	host: { '[@fadeInTransition]': '' }
})
export class LoginComponent implements OnInit {
	public spinner: Boolean = false;
	public loginFail: Boolean = false;

	constructor(private _router: Router
		,private _authenticationService:AuthenticationService
		,private _tokenService: TokenService) {}

	ngOnInit() {
		this._tokenService.removeAuthorization();
	}
	submitLogin(loginForm: NgForm){
		if(loginForm.valid){
			this.spinner = true;
			this.loginValidation(loginForm);
		}
	}
	loginValidation(loginForm: NgForm){
			this._authenticationService.authenticateUser({username:loginForm.value.email,password:loginForm.value.password}).subscribe((data: any) => {
				if(data.isAuthenticated)
				{
					this._tokenService.setToken(data.token);
					this._tokenService.setAuthorization();
					this._router.navigateByUrl('prospects')
					this.loginFail = false;
				}
				else{
					this.loginFail = true;
					this.spinner = false;
				}
			});
	}

}
