import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';

import { AppConstants } from '../app.constants';

@Injectable()
export class ResearchService {

  constructor(private _http: HttpClient) {

   }

   getProspectResearchDocuments(identifier:string): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Research/GetProspectResearchDocuments?identifier='+ identifier)
    .catch(this.handlError);
  }

  uploadDocument(newDocument:any): Observable<any> {
    return this._http.post(AppConstants.serviceEndpoint + 'Research/UploadDocument',newDocument)
    .catch(this.handlError);
  }

  downloadDocument(identifier:any): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Research/DownloadDocument?identifier='+ identifier)
    .catch(this.handlError);
  }

  private handlError(error: Response) {
    console.log(error);
    let message = `Error status code ${error.status} at ${error.url}`;
    return Observable.throw(message);
  }
}
