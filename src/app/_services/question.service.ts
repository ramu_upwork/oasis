import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';

import { AppConstants } from '../app.constants';

@Injectable()

export class QuestionService {
    
  constructor(private _http: HttpClient) {

  }

  getQuestions(verticalSectionId:number): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Questions/GetQuestions?sectionId='+ verticalSectionId)
    .catch(this.handlError);
  }

  getVerticals(): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Questions/GetVerticals')
    .catch(this.handlError);
  }

  saveQuestionaireResult(result): Observable<any> {
    return this._http.post(AppConstants.serviceEndpoint + 'Questions/SaveResult',result)
    .catch(this.handlError);
  }

  getQuestionaireResultSummary(prospectId:string): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Questions/GetQuestionaireResultSummary?prospectId='+prospectId)
    .catch(this.handlError);
  }

  generatePDf(images:any): Observable<any> {
    return this._http.post(AppConstants.serviceEndpoint + 'Questions/generatePDf',images)
    .catch(this.handlError);
  }

  saveSummaryPdf(pdf:any): Observable<any> {
    return this._http.post(AppConstants.serviceEndpoint + 'Questions/SaveSummaryPdf',pdf)
    .catch(this.handlError);
  }

  downloadSummaryPdf(id:number): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Questions/DownloadSummaryPdf?id='+ id)
    .catch(this.handlError);
  }
  
  private handlError(error: Response) {
    console.log(error);
    let message = `Error status code ${error.status} at ${error.url}`;
    return Observable.throw(message);
  }
}

