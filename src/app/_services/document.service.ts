import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';

import { AppConstants } from '../app.constants';

@Injectable()

export class DocumentService {
    
  constructor(private _http: HttpClient) {

  }

  getDocumentTypes(): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Documents/GetDocumentTypes')
    .catch(this.handlError);
  }

  uploadDocument(newDocument:any): Observable<any> {
    return this._http.post(AppConstants.serviceEndpoint + 'Documents/UploadDocument',newDocument)
    .catch(this.handlError);
  }

  getDocument(id:number): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Documents/GetDocument?id='+id)
    .catch(this.handlError);
  }
  
  private handlError(error: Response) {
    console.log(error);
    let message = `Error status code ${error.status} at ${error.url}`;
    return Observable.throw(message);
  }
}

