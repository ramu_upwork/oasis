import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';

import { AppConstants } from '../app.constants';
import { CookieService } from 'ngx-cookie-service';
@Injectable()

export class TokenService {

  //roles: any;
  constructor(private _http: Http, private cookieService: CookieService) {

  }
  
  public setToken(token: any) {
    this.cookieService.set(AppConstants._AuthToken, token);
  };

  public setAuthorization() {
    this.cookieService.set(AppConstants._Permissions, "SalesAssociate");
  };

  public removeAuthorization() {
    this.cookieService.deleteAll();
  }

  public getRoles() {
    return this.cookieService.get(AppConstants._Permissions);
  }

  private handlError(error: Response) {
    console.log(error);
    let message = `Error status code ${error.status} at ${error.url}`;
    return Observable.throw(message);
  }
}

