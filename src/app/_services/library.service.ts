import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';

import { AppConstants } from '../app.constants';

@Injectable()

export class LibraryService {
    
  constructor(private _http: HttpClient) {

  }

  sendWhitePaperMail(details:any): Observable<any> {
    return this._http.post(AppConstants.serviceEndpoint + 'Library/SendWhitePaper',details)
    .catch(this.handlError);
  }

  getLibrary(selectedFilter:any,searchParam:any): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Library/GetLibrary?typeFilter=' + selectedFilter+'&search='+ searchParam)
    .catch(this.handlError);
  }

  private handlError(error: Response) {
    console.log(error);
    let message = `Error status code ${error.status} at ${error.url}`;
    return Observable.throw(message);
  }
}

