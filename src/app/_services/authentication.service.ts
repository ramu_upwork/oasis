import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';

import { AppConstants } from '../app.constants';

@Injectable()

export class AuthenticationService {
    
  constructor(private _http: HttpClient) {

  }

  authenticateUser(credentials:any): Observable<any> {
    return this._http.post( AppConstants.serviceEndpoint + 'Authentication/ValidateUser',credentials)
    .catch(this.handlError);
  }

  getUser(): Observable<any> {
    return this._http.get( AppConstants.serviceEndpoint + 'Authentication/GetUser')
    .catch(this.handlError);
  }

  
  private handlError(error: Response) {
    console.log(error);
    let message = `Error status code ${error.status} at ${error.url}`;
    return Observable.throw(message);
  }
}

