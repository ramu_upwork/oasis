import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';

import { AppConstants } from '../app.constants';

@Injectable()

export class ProspectService {
    
  constructor(private _http: HttpClient) {

  }

  getProspects(selectedFilter:any,searchParam:any): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Prospects/GetProspects?statusFilter=' + selectedFilter+'&search='+ searchParam)
    .catch(this.handlError);
  }

  getProspect(identifier:any): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Prospects/GetProspect?identifier=' + identifier)
    .catch(this.handlError);
  }

  getProspectDocuments(identifier:any): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Prospects/GetProspectDocuments?identifier=' + identifier)
    .catch(this.handlError);
  }

  generateSummary(identifier:any): Observable<any> {
    return this._http.get(AppConstants.serviceEndpoint + 'Prospects/GenerateSummary?identifier=' + identifier)
    .catch(this.handlError);
  }

  sendZipFile(details:any): Observable<any> {
    return this._http.post(AppConstants.serviceEndpoint + 'Prospects/SendZipFile',details)
    .catch(this.handlError);
  }
  
  private handlError(error: Response) {
    console.log(error);
    let message = `Error status code ${error.status} at ${error.url}`;
    return Observable.throw(message);
  }
}

