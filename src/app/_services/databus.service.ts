import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';


import { Observable } from 'rxjs/Observable';
import { AppConstants } from '../app.constants';

@Injectable()
export class DataBusService {

  private defaultInfo: any = null;
  private defaultSummaryGeneratedStatus: any = {isGenerated:false,fileName:''};
  private defaultProspectHealthInfo: any = {isCompleted:false, isPdfCreated:false, resultId:0 };
  private defaultProspect: any = null;
  private defaultIsPdfGenerated: boolean = false;
 

  private examResult = new BehaviorSubject<any>(this.defaultInfo);
  private activeProspectSummaryGeneratedStatus = new BehaviorSubject<any>(this.defaultSummaryGeneratedStatus);
  private activeProspect = new BehaviorSubject<any>(this.defaultProspect);
  private activeProspectHealthInfo = new BehaviorSubject<any>(this.defaultProspectHealthInfo);
  private isPdfGenerated = new BehaviorSubject<any>(this.defaultIsPdfGenerated);
  
  ExamResult = this.examResult.asObservable();
  ActiveProspect = this.activeProspect.asObservable();
  ActiveProspectHealthInfo = this.activeProspectHealthInfo.asObservable();
  IsPdfGenerated = this.isPdfGenerated.asObservable();
  IsSummaryGenerated = this.activeProspectSummaryGeneratedStatus.asObservable();
  
  constructor() { 
  }

  updateExamResult(examData: any) {

    this.examResult.next(examData)

  }

  updateSummaryGeneratedStatus(data: any) {

    this.activeProspectSummaryGeneratedStatus.next(data)

  }

  setActiveProspect(prospsect: any) {

    this.activeProspect.next(prospsect)

  }

  setActiveProspectHealthInfo(prospsectHealthInfo: any) {

    this.activeProspectHealthInfo.next(prospsectHealthInfo)

  }

  updatePdfGenratedFlag(flag: boolean) {

    this.isPdfGenerated.next(flag)

  }
}