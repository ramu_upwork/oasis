import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule }        from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import * as _ from "lodash";

import { appRoutes } from './app.routes';
import { AppComponent } from './app.component';
import { RequestInterceptor } from './http.interceptor';
import { LoginComponent } from './login/login/login.component';
import { MainNavigationComponent } from './_navigation/main-navigation/main-navigation.component';
//import { DropdownDirective } from './_directives/dropdown.directive';
import { StyleguideComponent } from './styleguide/styleguide.component';
import { SelectricComponent } from './_jq-plugin/selectric/selectric.component';

import { SitemapComponent } from './sitemap/sitemap.component';
import { ErrorComponent } from './errors/error/error.component';

import { WhitePapersComponent } from './white-papers/white-papers.component';
import { SharedModule } from './_common/shared.module';
import { CookieService } from 'ngx-cookie-service';
import { TokenService } from './_services/token.service';
import { RouteManager } from './routeManager';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LibraryService } from './_services/library.service';
import { AuthenticationService } from './_services/authentication.service';
import { ProspectService } from './_services/prospect.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainNavigationComponent,
    StyleguideComponent,
    SelectricComponent,  
    SitemapComponent,
    ErrorComponent,
    WhitePapersComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    SharedModule.forRoot(),
    RouterModule.forRoot(appRoutes,{ useHash: true }),
    FormsModule
  ],
  providers: [
     { provide: LocationStrategy, useClass: HashLocationStrategy }
    ,{ provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true }
    ,CookieService
    ,TokenService
    ,AuthenticationService
    ,LibraryService
    ,RouteManager
    ,ProspectService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
