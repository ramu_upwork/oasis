import { trigger, state, animate, transition, style } from '@angular/animations';

export const fadeInOut =
// trigger name for attaching this animation to an element using the [@triggerName] syntax
trigger( 'fadeInOut', [
    state('true', style({
        opacity: 1,
        visibility: 'visible'
    })),
    state('false',   style({
        opacity: 0,
        visibility: 'hidden'
    })),
    transition('1 <=> 0', animate('200ms'))
    ]);


const tabSlideDuration = '.5s';
export const slideAnimation =
// trigger name for attaching this animation to an element using the [@triggerName] syntax
trigger( 'slideAnimation', [
    state('*', style({
        position: 'absolute',
        top: 0,
        margin: '0 auto',
        left: 0,
        right: 0
    })),
    transition('void => slideLtoR', [
        //void = element not in view
        style({
            transform: "translate3d(-100%,0,0)",
            opacity: 0
        }),
        // animation and styles at end of transition
        animate(tabSlideDuration + ' ease-in-out', style({
            transform: "translate3d(0,0,0)",
            opacity: 1
        }))
    ]),
    transition('slideLtoR => void', [
        style({
            transform: "translate3d(0,0,0)",
            opacity: 1
        }),
        animate(tabSlideDuration + ' ease-in-out', style({
            transform: "translate3d(100%,0,0)",
            opacity: 0
        }))
    ]),
    transition('void => slideRtoL', [
        style({
            transform: "translate3d(100%,0,0)",
            opacity: 0
        }),
        animate(tabSlideDuration + ' ease-in-out', style({
            transform: "translate3d(0,0,0)",
            opacity: 1
        }))
    ]),
    transition('slideRtoL => void', [
        style({
            transform: "translate3d(0,0,0)",
            opacity: 1
        }),
        animate(tabSlideDuration + ' ease-in-out', style({
            transform: "translate3d(-100%,0,0)",
            opacity: 0
        }))
    ])
]);




/*
//Tab Slide
export const tabSlideEnterLtoR =
trigger( 'tabSlideEnterLtoR', [
    state('*', style({
        position: 'absolute',
        top: 0,
        margin: '0 auto',
        left: 0,
        right: 0
    })),
    transition(':enter', [
        // styles at start of transition
        style({
            transform: "translate3d(-100%,0,0)"
        }),
        // animation and styles at end of transition
        animate(tabSlideDuration + ' ease-in-out', style({
            transform: "translate3d(0,0,0)"
        }))
        ])
    ]);
export const tabSlideEnterRtoL =
trigger( 'tabSlideEnterRtoL', [
    state('*', style({
        position: 'absolute',
        top: 0,
        margin: '0 auto',
        left: 0,
        right: 0
    })),
    transition(':enter', [
        // styles at start of transition
        style({
            transform: "translate3d(100%,0,0)"
        }),
        // animation and styles at end of transition
        animate(tabSlideDuration + ' ease-in-out', style({
            transform: "translate3d(0,0,0)"
        }))
        ])
    ]);
export const tabSlideLeaveRtoL =
trigger( 'tabSlideLeaveRtoL', [
    state('*', style({
        position: 'absolute',
        top: 0,
        margin: '0 auto',
        left: 0,
        right: 0
    })),
    transition(':leave', [
        // styles at start of transition
        style({
            transform: "translate3d(0,0,0)"
        }),
        // animation and styles at end of transition
        animate(tabSlideDuration + ' ease-in-out', style({
            transform: "translate3d(-100%,0,0)"
        }))
        ])
    ]);
export const tabSlideLeaveLtoR =
trigger( 'tabSlideLeaveLtoR', [
    state('*', style({
        position: 'absolute',
        top: 0,
        margin: '0 auto',
        left: 0,
        right: 0
    })),
    transition(':leave', [
        // styles at start of transition
        style({
            transform: "translate3d(0,0,0)"
        }),
        // animation and styles at end of transition
        animate(tabSlideDuration + ' ease-in-out', style({
            transform: "translate3d(100%,0,0)"
        }))
        ])
    ]);

*/