// import the required animation functions from the angular animations module
import { trigger, state, animate, transition, style } from '@angular/animations';
 
export const fadeInTransition =
    // trigger name for attaching this animation to an element using the [@triggerName] syntax
    trigger('fadeInTransition', [
 
        // end state styles for route container (host)
        state('*', style({
            // the view covers the whole screen with a semi tranparent background
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0
        })),

        // route 'enter' transition
        transition(':enter', [

            // styles at start of transition
            style({
                opacity: 0
            }),

            // animation and styles at end of transition
            animate('.3s ease-in-out', style({
               opacity: 1
            }))
        ]),

        // route 'leave' transition
        transition(':leave', [
            // animation and styles at end of transition
            style({
                opacity: 1
            }),

            // animation and styles at end of transition
            animate('.3s ease-in-out', style({
               opacity: 0
            }))
        ])
    ]);



export const modalTransition =
    trigger('modalTransition', [
        state('*', style({
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            zIndex: 9999
        })),
        transition(':enter', [
            style({
                opacity: 0
            }),
            animate('.3s ease-in-out', style({
               opacity: 1
            }))
        ]),
        transition(':leave', [
            style({
               opacity: 1
            }),
            animate('.3s ease-in-out', style({
               opacity: 0
            }))
        ])
    ]);



