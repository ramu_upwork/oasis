import { trigger, state, animate, transition, style } from '@angular/animations';

/*export const panelSearchGlobalGlobal =
    // trigger name for attaching this animation to an element using the [@triggerName] syntax
    trigger( 'panelSearchGlobalGlobal', [
        transition( ':enter', [
          style({transform: 'translateX(0%)'}),
          animate('.2s', style({transform: 'translateX(0%)'}))
        ]),
        transition( ':leave', [
          style({transform: 'translateX(0%)'}),
          animate('.2s', style({transform: 'translateX(0%)'}))
        ])
    ]);*/

export const panelSearchAnimation =
// trigger name for attaching this animation to an element using the [@triggerName] syntax
	trigger( 'panelSearchAnimation', [
		state('inactive', style({

      transform: 'scale(1)'
    })),
    state('active',   style({

      transform: 'scale(.1)'
    })),
    transition('inactive => active', animate('200ms ease-in')),
    transition('active => inactive', animate('200ms ease-out'))
	]);




export const panelSearchGlobalGlobal =
    // trigger name for attaching this animation to an element using the [@triggerName] syntax
    trigger( 'panelSearchGlobalGlobal', [
        transition( ':enter', [
          style({transform: 'translateX(100%)'}),
          animate('1s', style({transform: 'translateX(0)'}))
        ]),
        transition( ':leave', [
          style({transform: 'translateX(0%)'}),
          animate('1s', style({transform: 'translateX(10%)'}))
        ])
    ]);
