import { Component, OnInit } from '@angular/core';
import { fadeInTransition } from '../_animations/animations';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { EmailModalComponent } from '../shared/modals/email-modal/email-modal.component';
import { LibraryService } from '../_services/library.service';

@Component({
	selector: 'app-white-papers',
	templateUrl: './white-papers.component.html',
	styleUrls: ['./white-papers.component.scss'],
	animations: [fadeInTransition],
	host: { '[@fadeInTransition]': '' }
})
export class WhitePapersComponent implements OnInit {
	
	filterType: string = 'all';
	whitePapers:any = [];
	results:any = [];
	public activeFilter: any =null;
	searchParam:any='';
	private filters:any =[ {Id:0,Name:'View All'}, {Id:1,Name:'White Papers'}, {Id:2,Name:'Training'}];
	constructor(public dialog: MatDialog,
				private _libraryService:LibraryService) { }

	ngOnInit() {
		this.activeFilter = this.filters[0];
		this.getLibrary()
	}

	applyFilter(paper: any) {
		if(this.activeFilter.Id!=0)
		{
			return !paper.typeId != this.activeFilter.Id
		}
		else
		{
			return true;
		}
	}

	onSearchClick()
	{
		this.getLibrary()
	}

	setFilterType(filterType){
		for(var k = 0; k < this.filters.length; k++){
			if(filterType == this.filters[k].Id)
			{
				this.activeFilter = this.filters[k];
			}
		}	
	}

	getFilterType(filterType){
		if(filterType == this.activeFilter.Id){
			return true;
		}
	}

	emailModal() {
	    let emailModal = this.dialog.open(EmailModalComponent, {
	    	data: { context: 'lib' }
	    });
	    emailModal.afterClosed().subscribe(result => {
	      console.log('The dialog was closed');
	      //this.testResuts = result;
	    });
	 }

	 getLibrary() {
		this._libraryService.getLibrary(this.activeFilter.Id,this.searchParam).subscribe((data: any) => {
			this.whitePapers = data;
		});  
		}
}
