import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectGeneralComponent } from './prospect-general.component';

describe('ProspectGeneralComponent', () => {
  let component: ProspectGeneralComponent;
  let fixture: ComponentFixture<ProspectGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProspectGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
