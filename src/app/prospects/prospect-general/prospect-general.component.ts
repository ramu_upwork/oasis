import { Component, OnInit } from '@angular/core';
import { fadeInTransition } from '../../_animations/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { ProspectService } from '../../_services/prospect.service';
import { DataBusService } from '../../_services/databus.service';

@Component({
	selector: 'app-prospect-general',
	templateUrl: './prospect-general.component.html',
	styleUrls: ['./prospect-general.component.scss']
})
export class ProspectGeneralComponent implements OnInit {

	selectedProspectId : number;
	prospect:any = null;
	prospectHealthInfo:any = null;
	isLoaded:boolean = false;
	test: boolean = false;

	constructor( private route: ActivatedRoute
		,private _router: Router
		,private _prospectService:ProspectService
		,private _dataBus: DataBusService) {

		this.route.params.subscribe( params => {
			this.selectedProspectId = params.identifier || 0;
			if(this.selectedProspectId == 0)
			{
				this._router.navigateByUrl('prospects');
			}
			else
			{
				this._dataBus.setActiveProspect(this.selectedProspectId);
				this.getProspectDetails();
			}

		});

	}

	ngOnInit() {
		setTimeout( () => {
		this.test=true;
	}, 1000)
	}

	getProspectDetails() {
		this._prospectService.getProspect(this.selectedProspectId).subscribe((data: any) => {
			this._dataBus.setActiveProspect(data.prospect.details);
			this._dataBus.updateSummaryGeneratedStatus({isGenerated:data.prospect.details.isSummaryCreated,fileName:data.prospect.details.summaryFile});
			this._dataBus.setActiveProspectHealthInfo({isCompleted:data.prospect.healthCheck.isCompleted, isPdfCreated:data.prospect.healthCheck.isPdfCreated, resultId:data.prospect.healthCheck.resultId });
			this.prospect = data.prospect.details;	
			this.prospectHealthInfo = data.prospect.healthCheck; 		 
			this.isLoaded =true;
		});  

	};
}
