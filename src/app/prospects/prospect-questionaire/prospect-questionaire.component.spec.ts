import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectQuestionaireComponent } from './prospect-questionaire.component';

describe('ProspectQuestionaireComponent', () => {
  let component: ProspectQuestionaireComponent;
  let fixture: ComponentFixture<ProspectQuestionaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProspectQuestionaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectQuestionaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
