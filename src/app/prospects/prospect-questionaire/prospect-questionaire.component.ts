import { Component, OnInit, Inject } from '@angular/core';
import { fadeInTransition } from '../../_animations/animations';

import { QuestionService } from '../../_services/question.service';
import { DataBusService } from '../../_services/databus.service';
import { Router } from '@angular/router';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { QuestionResultsComponent } from '../question-results/question-results.component';
import { CloseQuestionnaireModalComponent } from '../../shared/modals/close-questionnaire-modal/close-questionnaire-modal.component';

@Component({
	selector: 'app-prospect-questionaire',
	templateUrl: './prospect-questionaire.component.html',
	styleUrls: ['./prospect-questionaire.component.scss']
})
export class ProspectQuestionaireComponent implements OnInit {

	activeProspect:any;
	sections:Array<any> = [];
	public totalQuestions: number = 0;
	
	public activeQuestion: number = 1;
	public activeSection: number = 1;
	public activeSectionObj: any = null;
	public activeQuestionObj: any = null;

	questions: Array<any> = [];
	isLoaded : boolean = false;
	isSubmited : boolean = false;
	isSubmitedAnim : boolean = false;
	isActiveQuestionAnswerSelected: boolean = false
	constructor( 
		private _questionService:QuestionService
		,private _dataBus: DataBusService
		,public dialog: MatDialog
		,private _router: Router
		,public thisModalRef: MatDialogRef<ProspectQuestionaireComponent>
		, @Inject(MAT_DIALOG_DATA) public data: any) { }



	ngOnInit() {
		this._dataBus.ActiveProspect.subscribe(activeProspect => 
		{
			this.activeProspect = activeProspect.uniqueId;
		});
		this.getQuestions(7)
	}

	close(){
		this.thisModalRef.close();
	}
	closeConfirm() {
		
	    let closeConfirm = this.dialog.open(CloseQuestionnaireModalComponent, {
	    	data: { name: 'TestData' }
	    });
	    closeConfirm.afterClosed().subscribe(result => {
	    	if(result == true){
	      		this.thisModalRef.close();
	    	}
	    });
	 }


	nextQuestion(){
		if(this.activeQuestion < this.totalQuestions){
			this.activeQuestion++;
			this.getActiveSection(this.activeQuestion);
		}
	}
	prevQuestion(){
		if(this.activeQuestion > 1){
			this.activeQuestion--;
			this.getActiveSection(this.activeQuestion);
		}
	}

	getActiveSection(activeQuestion){
		let breakLoop = false;
		for(var i = 0; i <  this.sections.length && !breakLoop; i++){
			for(var j = 0; j <  this.sections[i].question.length && !breakLoop; j++){
				if(this.sections[i].question[j].number == activeQuestion)
				{	
					this.activeQuestionObj = this.sections[i].question[j];
					this.activeSection = this.sections[i].number;
					this.activeSectionObj = this.sections[i]
					breakLoop = true;
					break;
				}
			}
		}
	};

	isAnswerSelected(qNum)
	{	
		if(qNum === this.activeQuestion){
			var answerSelected = false;	
			for(var k = 0; k < this.activeQuestionObj.answers.length && !answerSelected; k++){
				var currentAnswer = this.activeQuestionObj.answers[k];
				if(currentAnswer.isSelected =="true")
				{
					answerSelected = true;
				}
			}
			return answerSelected;
		}
	}

	onAnswerSelected(question , selectedAnswer,event)
	{
		if(selectedAnswer.isSelected=='false')
		{
			selectedAnswer.isSelected = 'true';
		}
		else
		{
			selectedAnswer.isSelected = 'false';
			event.target.checked = false;
		}
		
		for(var k = 0; k < question.answers.length; k++){
			var currentAnswer = question.answers[k];
			if(currentAnswer.answerId != selectedAnswer.answerId )
			{
				currentAnswer.isSelected ='false'
			}
		}
	}

	saveQuestionResult(questionaireResult)
	{
		setTimeout(() => {	
			let resultLoad : any = [];

			for(var k = 0; k < questionaireResult.length; k++){
				var obj = { sectionId:questionaireResult[k].sectionId, answers :[] }
				for(var i = 0; i < questionaireResult[k].answer.length; i++){
					obj.answers.push({questionId: questionaireResult[k].answer[i].questionId
						,answerId:questionaireResult[k].answer[i].answerId});
				}
				resultLoad.push(obj)
			}
			this._questionService.saveQuestionaireResult({prospectId: this.activeProspect , verticalSectionId:7, sections:resultLoad}).subscribe((data: any) => {
				this._dataBus.updateExamResult({VerticalSectionId:7, Result: questionaireResult});
				this._dataBus.updatePdfGenratedFlag(false);
				this._dataBus.setActiveProspectHealthInfo({isCompleted:true, isPdfCreated:false, resultId:0});
				let resultModal = this.dialog.open(QuestionResultsComponent, {
					data: { name: 'TestData' }
				});
				this.thisModalRef.close();
			}); 
		}, 3000)

	}

	submitQuestionaire()
	{
		this.isSubmitedAnim = true;
		setTimeout(() => {	
			this.isSubmited = true;
			let result: Array<any> = [];
			for(var i = 0; i <  this.sections.length; i++){
				var currentSection = this.sections[i];
				let section : any = {
					sectionId : currentSection.sectionId 
					,sectionName:currentSection.sectionName
					,answer : [] 
					,totalScore: 0,maxScore: (5 * currentSection.question.length)};

					for(var j = 0; j <  currentSection.question.length; j++){
					var currentQuestion = currentSection.question[j];
					for(var k = 0; k < currentQuestion.answers.length; k++){
						var currentAnswer = currentQuestion.answers[k];
						if(currentAnswer.isSelected =="true")
						{
							section.answer.push({questionId:currentQuestion.questionId, answerId:currentAnswer.answerId, point:currentAnswer.point});
							section.totalScore = section.totalScore + currentAnswer.point;
						}
					}
				}
				result.push(section);
			}

			this.saveQuestionResult(result);
		}, 500);
	};

	getQuestions(verticalSectionId:number) {
		this._questionService.getQuestions(verticalSectionId).subscribe((data: any) => {
			this.sections = data.sections;
			this.setActiveSection();
			this.setActiveQuestion();
			this.totalQuestions =  data.questiionsCount;
			this.isLoaded =true;
		});  
	};

	

	private setActiveSection()
	{
		if(this.sections.length >0 )
		{
			this.activeSection= this.sections[0].number;			
			this.activeSectionObj = this.sections[0];
		}

	};

	private setActiveQuestion()
	{
		if(this.sections.length >0 )
		{
			if(this.sections[0].question.length >0 )
			{
				this.activeQuestion = this.sections[0].question[0].number;
				this.activeQuestionObj = this.sections[0].question[0];
			}
		}
	};

}
