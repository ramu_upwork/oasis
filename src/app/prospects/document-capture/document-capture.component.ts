import { Component, OnInit, Inject } from '@angular/core';
import { fadeInTransition } from '../../_animations/animations';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { DataBusService } from '../../_services/databus.service';
import { ProspectService } from '../../_services/prospect.service';
import { EmailModalComponent } from '../../shared/modals/email-modal/email-modal.component';

@Component({
	selector: 'app-document-capture',
	templateUrl: './document-capture.component.html',
	styleUrls: ['./document-capture.component.scss']
})
export class ProspectsDocumentCaptureComponent implements OnInit {

	prospectDocuments:any=[];
	isLoaded:boolean =false;
	activeProspect:any;
	constructor( public dialog: MatDialog,
				public thisModalRef: MatDialogRef<ProspectsDocumentCaptureComponent>
				,@Inject(MAT_DIALOG_DATA) public data: any
				,private _prospectService:ProspectService
				,private _dataBus: DataBusService) { }

	ngOnInit() {

		this._dataBus.ActiveProspect.subscribe(activeProspect => 
			{
			  this.activeProspect = activeProspect.uniqueId;
			});

		this.getProspectDocuments();
	}

	close(){

	    this.thisModalRef.close();
	}

	getProspectDocuments() {
		this._prospectService.getProspectDocuments(this.activeProspect).subscribe((data: any) => {
			  this.prospectDocuments = data.documents;
			  this.isLoaded =true;
		});  
	};

	isDocumentFound(){
		var isDocumentFound = false;	
		for(var k = 0; k < this.prospectDocuments.length && !isDocumentFound; k++){
			for(var i = 0; i < this.prospectDocuments[k].subTypes.length && !isDocumentFound; i++){
					if(this.prospectDocuments[k].subTypes[i].documents.length > 0)
					{
						isDocumentFound =true;
					}
			}
			
		}
		return isDocumentFound;
	}

	sendZipFile(){
		if (this.isDocumentFound()) {
			let emailModal = this.dialog.open(EmailModalComponent, {
				data: { context: 'doc' }
			});
			emailModal.afterClosed().subscribe(result => {
			  console.log('The dialog was closed');
			  //this.testResuts = result;
			});
		}
	}

}
