import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentCaptureComponent } from './document-capture.component';

describe('DocumentCaptureComponent', () => {
  let component: DocumentCaptureComponent;
  let fixture: ComponentFixture<DocumentCaptureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentCaptureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentCaptureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
