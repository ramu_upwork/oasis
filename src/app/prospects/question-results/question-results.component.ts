import { Component, OnInit, Inject } from '@angular/core';
import { DataBusService } from '../../_services/databus.service';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { fadeInTransition, slideAnimation } from '../../_animations/animations';
import { QuestionService } from '../../_services/question.service';
import { ResultsPreviewComponent } from '../results-preview/results-preview.component';
import { UpdateQuestionnaireModalComponent } from '../../shared/modals/update-questionnaire-modal/update-questionnaire-modal.component';

@Component({
	selector: 'app-question-results',
	templateUrl: './question-results.component.html',
	styleUrls: ['./question-results.component.scss']
})
export class QuestionResultsComponent implements OnInit {

	
	currentTab: number = 0;
	activeProspect:any;
	verticals:Array<any> = [];
	verticalSectionId:number;
	selectedQuestionVertical:any = { name:'Vertical'};
	questionaireResults :any = [];
	radarChartLabels : string[]= [];
	radarChartData:any = [];
	summary:any;
	isSummaryLoaded : boolean = false;
	showGraph:boolean =false;
	radarChartType:string = 'radar';
	isPdfGenerated: boolean = false;
	isPdfGenerating: boolean = false;
	chartOptions:any = {
		animation: false,
		responsive: true,
		lineTension: 0,
		scale: {
			ticks: {
				display: false,
				maxTicksLimit: 5,
				min:0,
				max:100
			},
			gridLines: {
			  color: "#bcbcbc",
			  lineWidth: 2
			},
      },
		elements: {
			point:{				
				radius: 7,
				borderWidth: 0,
				hoverRadius: 7,
				hitRadius: 7
			}
		}
		
	};

	colors:any = [
		{
			backgroundColor: 'rgba(194, 219, 251, .5)',
			borderColor: 'rgba(41, 127, 202, 0.21)',
			pointBackgroundColor: 'rgba(50,121,205,1)',
			pointBorderColor: 'transparent',
			pointHoverBackgroundColor: 'rgba(50,121,205,.8)',
			pointHoverBorderColor: 'transparent'
		}
	];
				  
	constructor( private _dataBus: DataBusService
				,private _router: Router
				,public dialog: MatDialog
				,public thisModalRef: MatDialogRef<QuestionResultsComponent>
				, @Inject(MAT_DIALOG_DATA) public data: any
				, private _questionService:QuestionService) {

				 }

	ngOnInit() {

		this._dataBus.ActiveProspect.subscribe(activeProspect => 
			{
			  this.activeProspect = activeProspect.uniqueId;
			});

		this._dataBus.ExamResult.subscribe(updatedInfo => 
		{
			this.verticalSectionId = updatedInfo.VerticalSectionId;
			if(updatedInfo.Result == null)
			{
				this._router.navigateByUrl('prospects')
				this.thisModalRef.close();
			}
		});

		this.getSummary(this.activeProspect)
	}

	chartClicked(e:any):void {
		console.log(e);
	}

	chartHovered(e:any):void {
		console.log(e);
	}

	close(){
	    this.thisModalRef.close();
	}

	setTab(tab){
		this.currentTab = tab;
	}

	setVertical(selectedQuestionVertical) {
		this.selectedQuestionVertical = selectedQuestionVertical;
	}

	setChartColor(score:number)
	{
		var className = 'low';
		switch (true) {
			case score > 75:
			    className = 'high';
				break;
			case score>50 && score <=75:
				className = 'medium';
				break;
			case score>=0 && score <=50:
				className = 'low';
				break; 
		}
		return className;
	}

	getVertical(verticalId) {
		if(verticalId == this.selectedQuestionVertical.verticalSectionId){
			return true;
		}
	}

	getSummary (prospectId:string) {
		this._questionService.getQuestionaireResultSummary(prospectId).subscribe((data: any) => {
			  this.summary = data.summary;			  
			  let sectionPoints:number[]=[];
			  for(var k = 0; k < this.summary.length; k++){
					this.radarChartLabels.push(this.summary[k].verticalSectionName);
					sectionPoints.push(this.summary[k].totalScore);
			    }
			  this.radarChartData = [{data:sectionPoints,label:'Summary'}];
			  this.showGraph =true;
			  this.isSummaryLoaded = true;
		});  
	};

	generatePdf(){
		this.isPdfGenerating = true;
		setTimeout(() => {
			this.isPdfGenerated = true;
		}, 1500)
	}

	previewPdfModal(){
	    let previewPdfModal = this.dialog.open(ResultsPreviewComponent, { data: null });
	    previewPdfModal.afterClosed().subscribe(result => {
	      console.log('The dialog was closed');
	    });
	}

	restartQuestionnaireModal(){
	    let updateQuestionnaire = this.dialog.open(UpdateQuestionnaireModalComponent, {data: null });
	    updateQuestionnaire.afterClosed().subscribe(result => {
		  if(result)
		  {
				this.thisModalRef.close();
		  }
	    });

	}

}
