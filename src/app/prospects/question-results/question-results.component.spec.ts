import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionResultsComponent } from './question-results.component';

describe('QuestionResultsComponent', () => {
  let component: QuestionResultsComponent;
  let fixture: ComponentFixture<QuestionResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
