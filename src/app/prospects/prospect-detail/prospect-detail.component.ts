import { Component, OnInit } from '@angular/core';
import { fadeInTransition } from '../../_animations/animations';
import { DataBusService } from '../../_services/databus.service';

@Component({
	selector: 'app-prospect-detail',
	templateUrl: './prospect-detail.component.html',
	styleUrls: ['./prospect-detail.component.scss'],
	animations: [fadeInTransition],
	host: { '[@fadeInTransition]': '' }
})
export class ProspectDetailComponent implements OnInit {

	activeProspect: any;

	constructor(private _dataBus: DataBusService) { }

	ngOnInit() {
		this._dataBus.ActiveProspect.subscribe(activeProspect => {
			this.activeProspect = activeProspect;
		});
	}

}
