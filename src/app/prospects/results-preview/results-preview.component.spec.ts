import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsPreviewComponent } from './results-preview.component';

describe('ResultsPreviewComponent', () => {
  let component: ResultsPreviewComponent;
  let fixture: ComponentFixture<ResultsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
