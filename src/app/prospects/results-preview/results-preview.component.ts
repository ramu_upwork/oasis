import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DataBusService } from '../../_services/databus.service';
import { Router } from '@angular/router';
import { QuestionService } from '../../_services/question.service';
import html2canvas from 'html2canvas';
import { AppConstants } from '../../app.constants';
import domtoimage from 'dom-to-image';

@Component({
	selector: 'app-results-preview',
	templateUrl: './results-preview.component.html',
	styleUrls: ['./results-preview.component.scss']
})
export class ResultsPreviewComponent implements OnInit {

	resultId: number = 0;
	currentTab: number = 0;
	activeProspect: any;
	verticals: Array<any> = [];
	verticalSectionId: number;
	selectedQuestionVertical: any = { name: 'Vertical' };
	questionaireResults: any = [];
	radarChartLabels: string[] = [];
	radarChartData: any = [];
	summary: any;
	isSummaryLoaded: boolean = false;
	showGraph: boolean = false;
	radarChartType: string = 'radar';
	isPdfGenerated: boolean = false;
	isPdfGenerating: boolean = false;
	pages: any = [];
	chartOptions: any = {
		animation: false,
		responsive: true,
		lineTension: 0,
		scale: {
			ticks: {
				display: false,
				maxTicksLimit: 5,
				min:0,
				max:100
			},
			gridLines: {
				color: "#bcbcbc",
				lineWidth: 2
			},
		},
		elements: {
			point: {
				radius: 7,
				borderWidth: 0,
				hoverRadius: 7,
				hitRadius: 7
			}
		}

	};

	colors: any = [
		{
			backgroundColor: 'rgba(194, 219, 251, .5)',
			borderColor: 'rgba(41, 127, 202, 0.21)',
			pointBackgroundColor: 'rgba(50,121,205,1)',
			pointBorderColor: 'transparent',
			pointHoverBackgroundColor: 'rgba(50,121,205,.8)',
			pointHoverBorderColor: 'transparent'
		}
	];


	constructor(private _dataBus: DataBusService
		, private _router: Router
		, public dialog: MatDialog
		, public thisModalRef: MatDialogRef<ResultsPreviewComponent>
		, @Inject(MAT_DIALOG_DATA) public data: any
		, private _questionService: QuestionService) {

	}

	ngOnInit() {

		this._dataBus.ActiveProspect.subscribe(activeProspect => {
			this.activeProspect = activeProspect.uniqueId;
		});

		this._dataBus.IsPdfGenerated.subscribe(flag => {
			this.isPdfGenerated = flag;
		});

		this.getSummary(this.activeProspect)
	}

	chartClicked(e: any): void {
		console.log(e);
	}

	chartHovered(e: any): void {
		console.log(e);
	}

	close() {
		this.thisModalRef.close();
	}

	setTab(tab) {
		this.currentTab = tab;
	}

	setVertical(selectedQuestionVertical) {
		this.selectedQuestionVertical = selectedQuestionVertical;
	}

	setChartColor(score: number) {
		var className = 'low';
		switch (true) {
			case score > 75:
				className = 'high';
				break;
			case score > 50 && score <= 75:
				className = 'medium';
				break;
			case score >= 0 && score <= 50:
				className = 'low';
				break;
		}
		return className;
	}

	getVertical(verticalId) {
		if (verticalId == this.selectedQuestionVertical.verticalSectionId) {
			return true;
		}
	}

	getSummary(prospectId: string) {
		this._questionService.getQuestionaireResultSummary(prospectId).subscribe((data: any) => {
			this.summary = data.summary;
			this.resultId = data.id;
			let sectionPoints: number[] = [];
			for (var k = 0; k < this.summary.length; k++) {
				this.radarChartLabels.push(this.summary[k].verticalSectionName);
				sectionPoints.push(this.summary[k].totalScore);
			}
			this.radarChartData = [{ data: sectionPoints, label: 'Summary' }];
			this.showGraph = true;
			this.isSummaryLoaded = true;
		});
	};

	generatePdf() {
		this.isPdfGenerating = true;
		var pdfTopContentE = document.getElementById('pdfTopContent');
		domtoimage.toPng(pdfTopContentE)
			.then((dataUrl) => {
				this.pages.push({ Content: dataUrl, PageNumber: 1 })
				this.captureSectionImagesForPdf(0);
			})
			.catch(function (error) {
				console.error('oops, something went wrong!', error);
			});
	}

	captureSectionImagesForPdf(sectionIndex: number) {
		console.log(sectionIndex + " sectionIndex");
		console.log(this.summary.length + "  this.summary.length");
		if (sectionIndex < this.summary.length) {
			console.log("Entered loop");
			var htmlElement = document.getElementById('section' + sectionIndex);
			domtoimage.toPng(htmlElement)
				.then((dataUrl) => {
					console.log("Entered loop2");
					this.pages.push({ Content: dataUrl, PageNumber: sectionIndex + 2 })
					var pdfTopContent = dataUrl;
					this.captureSectionImagesForPdf(sectionIndex + 1);
				})
				.catch(function (error) {
					console.error('oops, something went wrong!', error);
				});
		}
		else {
			this.uploadPdfImageElements();
		}
	}

	uploadPdfImageElements() {
		this._questionService.saveSummaryPdf({ Id: this.resultId, Pages: this.pages }).subscribe((data: any) => {
			if (data > 0) {
				this._dataBus.updatePdfGenratedFlag(true);
				this._dataBus.setActiveProspectHealthInfo({ isCompleted: true, isPdfCreated: true, resultId: this.resultId });
				this._dataBus.updatePdfGenratedFlag(true);
				this.isPdfGenerated = true;
			}
		});
	}
	viewPdf() {
		var link = document.createElement('a');
		link.href = AppConstants.serviceEndpoint + 'Prospects/ViewHealthSummary?identifier=' + this.activeProspect;
		link.target = "_blank";
		document.body.appendChild(link);
		link.click();
	}


}
