import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { RouteManager } from '../routeManager';
import { SharedModule } from '../_common/shared.module';
import { ProspectService } from '../_services/prospect.service';
import { ProspectsListComponent } from './prospects-list/prospects-list.component';
import { ProspectDetailComponent } from './prospect-detail/prospect-detail.component';
import { ProspectGeneralComponent } from './prospect-general/prospect-general.component';
import { ProspectsDocumentCaptureComponent } from './document-capture/document-capture.component';
import { ProspectEditComponent } from './prospect-edit/prospect-edit.component';
import { ProspectQuestionaireComponent } from './prospect-questionaire/prospect-questionaire.component';
import { QuestionResultsComponent } from './question-results/question-results.component';
import { ResultsPreviewComponent } from './results-preview/results-preview.component';
import { QuestionService } from '../_services/question.service';
import { ChartsModule } from 'ng2-charts';
import { ProspectsResearchComponent } from './research/research.component';
import { DocumentService } from '../_services/document.service';
import { ResearchService } from '../_services/research.service';

const routes: Routes = [
    { 
         path: ''
        ,component: ProspectsListComponent
        //,canActivate: [RouteManager], data: { roles: ['Associate'] }
    },
    {
		 path: 'detail'
        ,component: ProspectDetailComponent
        ,children: [
            { 
                path: 'general/:identifier', 
                component: ProspectGeneralComponent
            },
            { 
                path: 'edit', 
                component: ProspectEditComponent
            },
            {
                path: 'document-capture',
                component: ProspectsDocumentCaptureComponent
            }]
		//,canActivate: [RouteManager], data: { roles: ['Associate'] }
	}
];

@NgModule({
  imports: [
     CommonModule
    ,FormsModule
    ,ChartsModule
    ,RouterModule.forChild(routes)
    ,SharedModule
  ],
  declarations: [
     ProspectsListComponent
    ,ProspectDetailComponent
    ,ProspectGeneralComponent
    ,ProspectsDocumentCaptureComponent
    ,ProspectEditComponent
    ,ProspectQuestionaireComponent
    ,QuestionResultsComponent
    ,ProspectsResearchComponent
    ,ResultsPreviewComponent
	],
    providers: [ProspectService,QuestionService,DocumentService,ResearchService],
    entryComponents: [
         ProspectQuestionaireComponent
        ,QuestionResultsComponent
        ,ProspectsResearchComponent
        ,ProspectsDocumentCaptureComponent
        ,ResultsPreviewComponent
     ],
})
export class ProspectModule {

 }