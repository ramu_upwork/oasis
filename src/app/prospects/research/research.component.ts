import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { fadeInTransition } from '../../_animations/animations';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ResearchService } from '../../_services/research.service';
import { DataBusService } from '../../_services/databus.service';
import { AppConstants } from '../../app.constants';
import { ProspectService } from '../../_services/prospect.service';

@Component({
	selector: 'app-research',
	templateUrl: './research.component.html',
	styleUrls: ['./research.component.scss']
	//,animations: [fadeInTransition]
	//,host: { '[@fadeInTransition]': '' }
})
export class ProspectsResearchComponent implements OnInit {

	public cards = [];
	isLoaded: boolean = false;
	activeProspect: any;
	isSummaryGenerating: boolean = false;
	summaryInfo: any;
	@ViewChild('myInput') myInputVariable: any;
	constructor(public thisModalRef: MatDialogRef<ProspectsResearchComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private _researchService: ResearchService,
		private _dataBus: DataBusService,
		private _prospectService: ProspectService) { }

	ngOnInit() {

		this._dataBus.ActiveProspect.subscribe(activeProspect => {
			this.activeProspect = activeProspect.uniqueId;
		});

		this._dataBus.IsSummaryGenerated.subscribe(newInfo => {
			this.summaryInfo = newInfo;
		});

		this.getProspectResearchDocuents();
	}

	isSectionSelected() {
		// var isSelected = false;	
		// for(var k = 0; k < this.cards.length && !isSelected; k++){
		// 	if(this.cards[k].isSelected == true)
		// 		{
		// 			isSelected = true;
		// 		}
		// }
		// return isSelected;
		return true;
	}

	getProspectResearchDocuents() {
		this._researchService.getProspectResearchDocuments(this.activeProspect).subscribe((data: any) => {
			this.cards = data;
			this.isLoaded = true;
		});
	};

	onFileChange(event, card) {
		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			let file = event.target.files[0];
			reader.readAsDataURL(file);
			reader.onload = () => {
				var document = reader.result;
				this.myInputVariable.nativeElement.value = "";
				this.uploadDocument(document, card)
			}
		}
	}

	isAnyDocumentUploaded() {
		debugger;
		var isDocumentFound = false;
		for (var k = 0; k < this.cards.length && !isDocumentFound; k++) {
			if (this.cards[k].document) {
				isDocumentFound = true;
			}
		}
		return isDocumentFound;
	}

	uploadDocument(document: any, card: any) {
		card.isUploading = true;
		this._researchService.uploadDocument({ prospectId: this.activeProspect, document: document, researchId: card.id }).subscribe((data: any) => {
			if (data.result.status > 0) {
				card.document = { uniqueId: data.result.documentId };
			}
		});
	};

	downloadResearchDocument(uniqueId: any) {
		var link = document.createElement('a');
		link.href = AppConstants.serviceEndpoint + 'Research/DownLoadDocument?identifier=' + uniqueId;
		link.target = "_blank";
		document.body.appendChild(link);
		link.click();
	};

	close() {
		this.thisModalRef.close();
	};

	generateSummary() {
		if (this.isAnyDocumentUploaded()) {
			this.isSummaryGenerating = true;
			this._prospectService.generateSummary(this.activeProspect).subscribe((data: any) => {
				if (data.isSummaryCreated) {
					this.summaryInfo.isGenerated = true;
					this.summaryInfo.fileName = data.fileName
					this._dataBus.updateSummaryGeneratedStatus({ isGenerated: true, fileName: data.fileName });
				}
			});
		}
	};

	viewSummary() {
		if (this.isAnyDocumentUploaded()) {
			var link = document.createElement('a');
			link.href = AppConstants.serviceEndpoint + 'Prospects/DownLoadSummary?identifier=' + this.summaryInfo.fileName;
			link.target = "_blank";
			document.body.appendChild(link);
			link.click();
		}
	}
}
