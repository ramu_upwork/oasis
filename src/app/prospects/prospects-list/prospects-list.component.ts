import { Component, OnInit } from '@angular/core';
import { LowerCasePipe } from '@angular/common';
import { fadeInTransition } from '../../_animations/animations';
import { ProspectService } from '../../_services/prospect.service';
import { Router } from '@angular/router';
import { DataBusService } from '../../_services/databus.service';

@Component({
	selector: 'app-prospects-list',
	templateUrl: './prospects-list.component.html',
	styleUrls: ['./prospects-list.component.scss'],
	animations: [fadeInTransition],
	host: { '[@fadeInTransition]': '' }
})
export class ProspectsListComponent implements OnInit {

	public activeFilter: any =null;
	public searchParam: any = '';
	private filters:any =[ {Id:0,Name:'All'}, {Id:1,Name:'Pending'}, {Id:2,Name:'New'}, {Id:3,Name:'Open'} ];
	prospects: Array<any> = [];
	prospectStatus: Array<any> = [];
	isLoaded : boolean = false;




	constructor( private _prospectService:ProspectService
		,private _router: Router
		,private _dataBus: DataBusService ) { 

	}

	ngOnInit() {

		this.activeFilter = this.filters[0];
		this.getProspects();
	}

	onProspectClick (selectedProspect){
		//this._dataBus.setActiveProspect(selectedProspect.uniqueId);		
		this._router.navigateByUrl('/prospects/detail/general/'+selectedProspect.uniqueId)
	}

	setFilterType(filterType){
		for(var k = 0; k < this.filters.length; k++){
			if(filterType == this.filters[k].Id)
			{
				this.activeFilter = this.filters[k];
			}
		}
		this.isLoaded = false;
		this.getProspects();
	}

	onSearchClick()
	{
		this.isLoaded = false;
		this.getProspects();
	}

	getFilterType(filterType){
		if(filterType == this.activeFilter.Id){
			return true;
		}
	}

	getProspects() {
		this._prospectService.getProspects(this.activeFilter.Id,this.searchParam).subscribe((data: any) => {
			this.prospects = data.prospects;
			this.isLoaded =true;
		});  
	};

}
