import { Component, OnInit } from '@angular/core';
import { fadeInTransition, fadeInOut } from '../_animations/animations';

@Component({
  selector: 'app-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['./sitemap.component.scss'],
  animations: [fadeInTransition, fadeInOut],
	host: { '[@fadeInTransition]': '' }
})
export class SitemapComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
