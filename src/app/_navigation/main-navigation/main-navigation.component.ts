import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TokenService } from '../../_services/token.service';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
	selector: 'app-main-navigation',
	templateUrl: './main-navigation.component.html',
	styleUrls: ['./main-navigation.component.scss']
})
export class MainNavigationComponent implements OnInit {
	searchOpen: boolean;
	searchText: string;
	userName:string='';
	constructor(private _router: Router
		,private activatedRoute: ActivatedRoute
		,private _tokenService: TokenService,
		private _authService:AuthenticationService) {

	}


	ngOnInit() {
		this.searchOpen = false;
		this.getUser();
	}

	toggleSearch(){
		this.searchOpen = !this.searchOpen;
	}
	openSearch(){
		this.searchOpen = true;
	}
	closeSearch(){
		this.searchOpen = false;
	}

	onLogOut()
	{
		this._tokenService.removeAuthorization();
		this._router.navigateByUrl('/')
	}

	getUser() {
		this._authService.getUser().subscribe((data: any) => {
			this.userName = data;
		});

	};




}
