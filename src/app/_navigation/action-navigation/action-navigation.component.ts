import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { ProspectsResearchComponent } from '../../prospects/research/research.component';
import { ProspectQuestionaireComponent } from '../../prospects/prospect-questionaire/prospect-questionaire.component';
import { DataBusService } from '../../_services/databus.service';
import { ProspectsDocumentCaptureComponent } from '../../prospects/document-capture/document-capture.component';
import { ProspectService } from '../../_services/prospect.service';
import { AppConstants } from '../../app.constants';
import { ResultsPreviewComponent } from '../../prospects/results-preview/results-preview.component';
import { ConfirmModalComponent } from '../../shared/modals/confirm-modal/confirm-modal.component';


@Component({
  selector: 'action-navigation',
  templateUrl: './action-navigation.component.html',
  styleUrls: ['./action-navigation.component.scss']
})
export class ActionNavigationComponent implements OnInit {

  activeProspect: any;
  summaryInfo: any;
  activeProspectHealthInfo: any;
  isInitialized: boolean = false;
  isSummaryGenerating: boolean = false;
  isCompanyHealthGenerating: boolean = false;
  constructor(public dialog: MatDialog, private _dataBus: DataBusService, private _prospectService: ProspectService) { }

  ngOnInit() {

    this._dataBus.ActiveProspect.subscribe(activeProspect => {
      this.activeProspect = activeProspect.uniqueId;
    });

    this._dataBus.IsSummaryGenerated.subscribe(newInfo => {
      this.summaryInfo = newInfo;
    });

    this._dataBus.ActiveProspectHealthInfo.subscribe(healthInfo => {
      this.activeProspectHealthInfo = healthInfo;
    });

    this.isInitialized=true;
  }

  researchModal() {

    let researchModal = this.dialog.open(ProspectsResearchComponent, {
      data: { name: 'TestData' }
    });
    researchModal.afterClosed().subscribe(result => {
    });
  }

  questionaireModal() {
    if (this.activeProspectHealthInfo.isCompleted) {
      let closeConfirm = this.dialog.open(ConfirmModalComponent, {
        data: { Heading: 'Health Summary', Message: 'The existing Company Health Report will be replaced, do you wish to continue?' }
      });
      closeConfirm.afterClosed().subscribe(result => {
        if (result == true) {
          let questionaireModal = this.dialog.open(ProspectQuestionaireComponent, {
            data: { name: 'TestData' }
          });
          questionaireModal.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
          });
        }
      });
    }
    else {
      let questionaireModal = this.dialog.open(ProspectQuestionaireComponent, {
        data: { name: 'TestData' }
      });
      questionaireModal.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    }
  }

  documentCaptureModal() {
    let documentCaptureModal = this.dialog.open(ProspectsDocumentCaptureComponent, {
      data: { name: 'TestData' }
    });
    documentCaptureModal.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  generateSummary() {
    this.isSummaryGenerating = true;
    this._prospectService.generateSummary(this.activeProspect).subscribe((data: any) => {
      if (data.isSummaryCreated) {
        this.summaryInfo.isGenerated = true;
        this.summaryInfo.fileName = data.fileName
        this._dataBus.updateSummaryGeneratedStatus({ isGenerated: true, fileName: data.fileName });
      }
    });
  }

  viewSummary() {
    var link = document.createElement('a');
    link.href = AppConstants.serviceEndpoint + 'Prospects/DownLoadSummary?identifier=' + this.summaryInfo.fileName;
    link.target = "_blank";
    document.body.appendChild(link);
    link.click();
  }

  previewCompanyHealth() {
    this.isCompanyHealthGenerating = true;
    let previewPdfModal = this.dialog.open(ResultsPreviewComponent, { data: null });
    previewPdfModal.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.isCompanyHealthGenerating = false;
    });
  }

  viewCompanyHealth() {
    var link = document.createElement('a');
    link.href = AppConstants.serviceEndpoint + 'Prospects/ViewHealthSummary?identifier='+ this.activeProspect;
    link.target = "_blank";
    document.body.appendChild(link);
    link.click();
  }

}
