export class AppConstants {
  public static serviceEndpoint = "http://localhost:13825/api/";
  public static _AuthToken = "OasisSalesToken";
  public static _Permissions = "OasisSalesPermissions";
  public static _LocalCache = "OasisSalesLocalCache";
}
