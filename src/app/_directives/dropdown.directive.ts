import { Directive, ElementRef, Renderer, HostListener, Input } from '@angular/core';
import * as $ from "jquery";

@Directive({
	selector: '[dropdown]'
})
export class DropdownDirective {
	public toggle;
	public toggler;
	public toggled;
	constructor(private el: ElementRef, private renderer: Renderer) {
	}

	ngOnInit() {
		this.toggle = $(this.el.nativeElement);
		this.toggler = $(this.el.nativeElement).find('[dropdownToggler]');
		this.toggled = $(this.el.nativeElement).find('[dropdownToggled]');
	}

	ngAfterViewInit() {
		let toggle = this.toggle;
		let toggler = this.toggler;
		let toggled = this.toggled;

		function close(){
			toggler.removeClass('is-open');
			toggled.removeClass('is-open');
		}

		toggler.click(function(){
			//$(this).next('[dropdownToggled]').stop().slideToggle();
			$(this).next('[dropdownToggled]').toggleClass('is-open');
			$(this).toggleClass('is-open');
		});
		toggled.find('[dropdownClose]').click(function(){
			close();
		});


		$(document).bind('click', function(e){
			if(e.target != toggle && !toggle.has(e.target).length){
				close();
			}
		});
	}
}
