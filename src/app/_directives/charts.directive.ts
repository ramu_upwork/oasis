import { Directive, ElementRef, Renderer, HostListener, Input } from '@angular/core';
import * as $ from "jquery";


@Directive({
	selector: '[chart-circle]'
})
export class ChartsDirective {
	public wrap;
	public svg;
	public circleBg;
	public stats;
	@Input() chartCirclePct: number;

	constructor(private _elRef: ElementRef, private _renderer: Renderer) {
	}

	ngOnInit() {
		console.log('test!')
		this.svg = $(this._elRef.nativeElement).find('svg');
		this.wrap = $(this._elRef.nativeElement);
		this.circleBg = $(this._elRef.nativeElement).find('[chart-circle-bg]');
		this.stats = $(this._elRef.nativeElement).find('[chart-circle-stats]');
		console.log(this.chartCirclePct)
	}

	ngAfterViewInit() {
		let width = this.wrap.width();
		let height = this.wrap.height();


		//pure js solution
		//this._renderer.setElementAttribute(this._elRef.nativeElement.querySelector('svg'), 'viewBox', '0 0 '+ width +' '+ height);

		this.svg.attr('viewBox', '0 0 '+ width +' '+ height);
		this.svg.css({
			'width': width,
			'height': height
		});

		let strokeLength1 = width * Math.PI * this.chartCirclePct / 100;
		let strokeLength2 = width * Math.PI;
		let strokeLengthTotal = strokeLength1 + " " + strokeLength2; 

		this.circleBg.css({
			'cx': (width/2),
			'cy': (width/2),
			'r': (width >= 40 ? width/2 - 1 : width/2)
		});

		this.stats.css({
			'cx': (width/2),
			'cy': (width/2),
			'r': (width/2),
			'stroke-dasharray': strokeLengthTotal
		});
	}

}
