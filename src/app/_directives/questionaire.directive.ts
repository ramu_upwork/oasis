import { Directive, ElementRef, Renderer, Input, OnChanges, HostListener } from '@angular/core';
import * as $ from "jquery";

@Directive({
	selector: '[scroll-questionaire]'
})
export class QuestionaireDirective implements OnChanges{

	public questionaire;
	public questionaireWrap;
	public questionaireForm;

	private questionaireH;
	private questionaireTop;
	private blueBar;
	private blueBarH: number;
	
	@Input() activeQuestion: number;
	@Input() activeSection: number;
	@Input() totalQuestions: number;
	@Input() isLoaded: boolean;

	private prevQuestion: number = this.activeQuestion;



	constructor(private el: ElementRef, private renderer: Renderer) {
	}

	ngOnInit() {
		this.questionaire = $(this.el.nativeElement);
		this.questionaireWrap = $(this.el.nativeElement).find('[questionaire-wrap]');
		this.questionaireForm = $(this.el.nativeElement).find('[questinaire-form]');
		this.blueBar = $(this.el.nativeElement).find('.m-questionaire__bg-bar');

		this.updateVariables();


	}
	ngAfterViewInit() {
		this.prevQuestion = this.activeQuestion;
		this.updateVariables();
		this.getActiveQuestion();
		this.updateTitlesPosition();
		
	}
	ngOnChanges(changes){
		if(changes.activeQuestion){
			this.getActiveQuestion();
		}
		if(changes.activeSection){
			this.updateTitlesPosition();
		}
		if(changes.isLoaded){
			setTimeout(() => {			
				//console.log('test')
				this.updateVariables();
				this.getActiveQuestion();
				this.sectionActive();
			}, 800)
		}
	}

	updateVariables(){
		this.questionaireH = this.questionaire.innerHeight() ? this.questionaire.innerHeight() : this.questionaire.height();
		this.questionaireTop = this.questionaire.offset().top;

		this.blueBarH = this.blueBar.innerHeight();

	}

	getActiveQuestion(){
		let activeQ = $('#questionId-'+this.activeQuestion);
		let activeQSection = activeQ.parents('.m-questionaire__form__section');
		let activeQSectionTop;
		let activeQTop;
		let newPosition = 0;
		let hiddenBlock = $('.m-questionaire__wrap__hidden-50').innerHeight();//for scrolling starting at 50%
		
		let wrapperH = this.questionaireH;
		let windowH = $(window).height();

		
		
		if (activeQ.length) {
			activeQTop = activeQ.position().top;
			activeQSectionTop = activeQSection.position().top;

			newPosition = -activeQTop - activeQSectionTop + this.questionaireH / 2 - this.blueBarH / 2 - 60 - hiddenBlock;
			if(this.prevQuestion > this.activeQuestion){
				newPosition = -activeQTop - activeQSectionTop + this.questionaireH / 2 - this.blueBarH / 2 - 60 - hiddenBlock - 200; //-200 for css transform
			}
			
			//this.questionaireWrap.css('transform', 'translate3d(0px, '+newPosition+'px, 0)')
			//this.questionaireWrap.css('top', newPosition+'px')
			this.questionaireWrap.stop().animate({
				scrollTop: -newPosition
			}, 500);
			
			this.prevQuestion = this.activeQuestion;
		}
	}

	updateTitlesPosition(){
		let base = this;
		let sections = $('.m-questionaire__form__section');

		let currentSection = $('#sectionId-'+base.activeSection);
		let currentTitle = currentSection.find('.m-questionaire__form__section__title');


		//currentTitle.addClass('is-active');

		if(this.questionaireWrap){
			this.questionaireWrap.bind( 'scroll', function () {
				base.sectionActive();
			})
		}
	}

	isSectionActive(section){
		let blueBarHalf = this.blueBarH / 2;
		let questionaireH = this.questionaireH;
		let questionaireTop = this.questionaireTop;
		let center = questionaireH / 2;
		let title = section.find('.m-questionaire__form__section__title');
		let titleH = title.height()

		let sectionTop = section.offset().top - questionaireTop;
		let titleTop = sectionTop + titleH/2;
		let sectionBottom = section.offset().top + section.height() - questionaireTop;
		let titleBottom = sectionBottom - titleH/2;

		if(titleTop <= center + blueBarHalf && titleBottom >= center - blueBarHalf){
			title.addClass('is-on-blue-bar');
		}
		else{
			title.removeClass('is-on-blue-bar');
		}


		if(titleTop <= center && titleBottom >= center){
			return 'center';
		}
		else if(titleTop > center){
			return 'top';
		}
		else{
			return 'bottom';
		}
	}

	sectionActive(){
		let base = this;
		let sections = $('.m-questionaire__form__section');
		sections.each(function(index, value){
			if(base.isSectionActive($(value)) === 'center'){
				base.fixTitle($(value));
			}
			else if(base.isSectionActive($(value)) === 'top'){
				base.unfixTitleTop($(value));
			}
			else if(base.isSectionActive($(value)) === 'bottom'){
				base.unfixTitleBottom($(value));
			}
		})
	}


	fixTitle(section){
		//center with blue bar
		let title = section.find('.m-questionaire__form__section__title');
		let titleH = title.height();
		title.addClass('is-active');

		if($('html').hasClass("no-flexboxlegacy")){
			let questionnaireH = this.questionaireWrap.height();
			let questionnaireTop = this.questionaireWrap.offset().top;
			let calc = questionnaireTop + questionnaireH/2;
			title.css('top', calc+'px');
		}
		else{
			let blueBarH = this.blueBarH;
			let blueBarHalf = this.blueBarH / 2;
			let blueBarTop = this.blueBar.position().top;
			let calc = blueBarTop + blueBarHalf - titleH / 2;
			title.css('top', calc+'px');
		}
		


	}
	unfixTitleBottom(section){
		let title = section.find('.m-questionaire__form__section__title');
		let sectionH = section.height();
		let titleH = title.height();
		let calc = sectionH - titleH;
		title.removeClass('is-active');		
		title.css('top', calc+'px')

	}
	unfixTitleTop(section){
		let title = section.find('.m-questionaire__form__section__title');
		title.removeClass('is-active');
		title.css('top', 0);
	}



	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.updateVariables();
		this.getActiveQuestion();
		this.sectionActive();
	}





}






