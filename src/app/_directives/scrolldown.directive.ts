import { Directive, ElementRef, Renderer, HostListener, Input } from '@angular/core';
import * as $ from "jquery";

@Directive({
	selector: '[scrolldown]'
})
export class ScrolldownDirective {
	
	private trigger;
	constructor(private el: ElementRef, private renderer: Renderer) {
		this.trigger = $(this.el.nativeElement);
	}

	ngOnInit() {

	}

	ngAfterViewInit() {

		this.trigger.click(function(e){
			e.preventDefault();
			var addressValue = $(this).attr("href");
			console.log(addressValue);
			var positionScroll = $(addressValue).offset().top;
			if($(this).closest(".page__wrap").length ) {
				$('.page__wrap').animate({scrollTop: positionScroll - 30},'slow');
			}
			else if($(this).closest(".modal__wrap").length ) {
				$('.modal__wrap').animate({scrollTop: positionScroll - 30},'slow');
			}
		})
	}
}
