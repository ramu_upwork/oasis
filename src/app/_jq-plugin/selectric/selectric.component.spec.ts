import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectricComponent } from './selectric.component';

describe('SelectricComponent', () => {
  let component: SelectricComponent;
  let fixture: ComponentFixture<SelectricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
