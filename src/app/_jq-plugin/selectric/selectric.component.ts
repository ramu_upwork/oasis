import { ViewChild, ElementRef, AfterViewInit, Component, Input  } from '@angular/core';
//import * as $ from "jquery";
declare var jQuery: any;


@Component({
	selector: 'selectric',
	templateUrl: './selectric.component.html'
})
export class SelectricComponent implements AfterViewInit {

	@ViewChild('select') input: ElementRef;
	@Input() options = '';

	ngAfterViewInit() {
		jQuery(this.input.nativeElement).selectric();
	}

}
