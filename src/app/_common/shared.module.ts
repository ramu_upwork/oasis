import { RouterModule } from '@angular/router';
import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';

import * as $ from "jquery";

import { QuestionaireDirective } from '../_directives/questionaire.directive';
import { MarqueeComponent } from '../shared/marquee/marquee.component';
import { ProgressResultsComponent } from '../shared/progress-results/progress-results.component';
import { MenuNavigationComponent } from '../_navigation/menu-navigation/menu-navigation.component';
import { ActionNavigationComponent } from '../_navigation/action-navigation/action-navigation.component';
import { DocumentAddComponent } from '../shared/document-add/document-add.component';
import { ChartCircleComponent } from '../shared/chart-circle/chart-circle.component';
import { DataBusService } from '../_services/databus.service';
import { ProgressQuestionaireComponent } from '../shared/progress-questionaire/progress-questionaire.component';
import { DropdownDirective } from '../_directives/dropdown.directive';
import { ScrolldownDirective } from '../_directives/scrolldown.directive';
import { ChartsDirective } from '../_directives/charts.directive';
import { PageLoaderComponent } from '../shared/loaders/page-loader/page-loader.component';
import { TableLoaderComponent } from '../shared/loaders/table-loader/table-loader.component';
import { WebCamModule } from 'ack-angular-webcam';
import { FullImageModalComponent } from '../shared/modals/full-image-modal/full-image-modal.component';
import { EmailModalComponent } from '../shared/modals/email-modal/email-modal.component';
import { UpdateQuestionnaireModalComponent } from '../shared/modals/update-questionnaire-modal/update-questionnaire-modal.component';
import { CloseQuestionnaireModalComponent } from '../shared/modals/close-questionnaire-modal/close-questionnaire-modal.component';
import { ConfirmModalComponent } from '../shared/modals/confirm-modal/confirm-modal.component';

@NgModule({
  declarations: [
     QuestionaireDirective
    ,DropdownDirective
    ,ScrolldownDirective
    ,ChartsDirective
    ,MarqueeComponent
    ,ProgressResultsComponent
    ,MenuNavigationComponent
    ,ActionNavigationComponent
    ,DocumentAddComponent
    ,ChartCircleComponent
    ,ProgressResultsComponent
    ,ProgressQuestionaireComponent
    ,PageLoaderComponent
    ,TableLoaderComponent
    ,FullImageModalComponent
    ,EmailModalComponent
    ,UpdateQuestionnaireModalComponent
    ,CloseQuestionnaireModalComponent
    ,ConfirmModalComponent
    ],
  imports: [
     RouterModule
    ,FormsModule
    ,CommonModule
    ,MatDialogModule
    ,WebCamModule
  ],
  exports: [
     QuestionaireDirective
    ,DropdownDirective
    ,ScrolldownDirective
    ,ChartsDirective
    ,MarqueeComponent
    ,ProgressResultsComponent
    ,MenuNavigationComponent
    ,ActionNavigationComponent
    ,DocumentAddComponent
    ,ChartCircleComponent
    ,ProgressResultsComponent
    ,ProgressQuestionaireComponent
    ,PageLoaderComponent
    ,TableLoaderComponent
  ],
  entryComponents: [
    FullImageModalComponent
    ,EmailModalComponent
    ,UpdateQuestionnaireModalComponent       
    ,CloseQuestionnaireModalComponent
    ,ConfirmModalComponent
  ],
})

export class SharedModule {
  
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [ DataBusService ]
    };
  }  

  constructor(){ }
}
