import { Component, OnInit, Input } from '@angular/core';
import { DataBusService } from '../../_services/databus.service';

@Component({
	selector: 'header-marquee',
	templateUrl: './marquee.component.html',
	styleUrls: ['./marquee.component.scss'],
	host: {'class': 'm-marquee'}
})
export class MarqueeComponent implements OnInit {

	@Input() title:string; 
	activeProspect: any;
	constructor(private _dataBus: DataBusService) { }

	ngOnInit() {

		this._dataBus.ActiveProspect.subscribe(activeProspect => {
			this.activeProspect = activeProspect;
		  });
	}

}
