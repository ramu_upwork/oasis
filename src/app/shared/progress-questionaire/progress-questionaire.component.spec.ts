import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressQuestionaireComponent } from './progress-questionaire.component';

describe('ProgressQuestionaireComponent', () => {
  let component: ProgressQuestionaireComponent;
  let fixture: ComponentFixture<ProgressQuestionaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressQuestionaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressQuestionaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
