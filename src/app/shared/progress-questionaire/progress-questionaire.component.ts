import { Component, OnInit, Input, AfterViewInit } from '@angular/core';

@Component({
	selector: 'progress-questionaire',
	templateUrl: './progress-questionaire.component.html',
	styleUrls: ['./progress-questionaire.component.scss'],
	host: {'class': 'progress-questionaire'}
})
export class ProgressQuestionaireComponent implements AfterViewInit {

	@Input() sections;
	@Input() activeSection;
	@Input() activeQuestion;
	@Input() isSubmited;

	constructor() { }

	ngAfterViewInit() {
		
	}

	
	updateWidth(section){
		let breakLoop = false;
		if(section.number == this.activeSection && !this.isSubmited){
			let qLength = section.question.length;
			for(var i = 0; i < qLength; i++){
				if(this.activeQuestion == section.question[i].number && !breakLoop){
					let qCurrent = section.question[i].questionRank;
					let percent = (qCurrent - 1)/qLength*100;
					
					breakLoop = false;
					return percent+"%";
				}
			}
		}
		else if(this.isSubmited){
			return "100%";
		}
	}


}
