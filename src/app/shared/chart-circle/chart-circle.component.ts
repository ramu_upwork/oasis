import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'chart-circle',
	templateUrl: './chart-circle.component.html',
	styleUrls: ['./chart-circle.component.scss'],
	host: {'class': 'chart-circle'}
})
export class ChartCircleComponent implements OnInit {
	@Input() chartValue;
	constructor() { }

	ngOnInit() {

	}

}
