import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartCircleComponent } from './chart-circle.component';

describe('ChartCircleComponent', () => {
  let component: ChartCircleComponent;
  let fixture: ComponentFixture<ChartCircleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartCircleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
