import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgForm } from '@angular/forms';
import { LibraryService } from '../../../_services/library.service';
import { ProspectService } from '../../../_services/prospect.service';

@Component({
  selector: 'app-email-modal',
  templateUrl: './email-modal.component.html',
  styleUrls: ['./email-modal.component.scss']
})
export class EmailModalComponent implements OnInit {

  isMailSending: boolean = false;
  isMailSendingCompleted: boolean = false;
  constructor(
    private _libraryService: LibraryService,
    private _prospectService: ProspectService,
    public thisModalRef: MatDialogRef<EmailModalComponent>
    , @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

  submitEmail(emailForm: NgForm) {
    if (emailForm.valid) {
      this.isMailSending = true;
      if (this.data.context == 'lib') {
        this._libraryService.sendWhitePaperMail({ ReceipientEmail: emailForm.value.receipientEmail }).subscribe((data: any) => {
          if (data) {
            this.isMailSending = false;
            this.isMailSendingCompleted = true;
          }
        });
      }
      else if (this.data.context == 'doc') {
        this._prospectService.sendZipFile({ ReceipientEmail: emailForm.value.receipientEmail }).subscribe((data: any) => {
          if (data) {
            this.isMailSending = false;
            this.isMailSendingCompleted = true;
          }
        });
      }
      else {
        this.isMailSending = false;
        this.isMailSendingCompleted = true;
      }
    }
  }

  close() {
    this.thisModalRef.close();
  }


}
