import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
	selector: 'app-full-image-modal',
	templateUrl: './full-image-modal.component.html',
	styleUrls: ['./full-image-modal.component.scss']
})
export class FullImageModalComponent implements OnInit {

	constructor( public thisModalRef: MatDialogRef<FullImageModalComponent>
				,@Inject(MAT_DIALOG_DATA) public data: any){}

	ngOnInit() {
		
	}

	close(){
		this.thisModalRef.close();
	}

}
