import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseQuestionnaireModalComponent } from './close-questionnaire-modal.component';

describe('CloseQuestionnaireModalComponent', () => {
  let component: CloseQuestionnaireModalComponent;
  let fixture: ComponentFixture<CloseQuestionnaireModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloseQuestionnaireModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseQuestionnaireModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
