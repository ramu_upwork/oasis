import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgForm } from '@angular/forms';
import { LibraryService } from '../../../_services/library.service';


@Component({
	selector: 'app-close-questionnaire-modal',
	templateUrl: './close-questionnaire-modal.component.html',
	styleUrls: ['./close-questionnaire-modal.component.scss']
})
export class CloseQuestionnaireModalComponent implements OnInit {

	constructor( private _libraryService:LibraryService,
		public thisModalRef: MatDialogRef<CloseQuestionnaireModalComponent>
		,@Inject(MAT_DIALOG_DATA) public data: any){}

	ngOnInit() {
	}



	close(){
		this.thisModalRef.close();
	}
}
