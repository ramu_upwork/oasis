import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgForm } from '@angular/forms';
import { LibraryService } from '../../../_services/library.service';


@Component({
	selector: 'confirm-modal',
	templateUrl: './confirm-modal.component.html',
	styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {

	modalTest:any=null;
	constructor(public thisModalRef: MatDialogRef<ConfirmModalComponent>
		,@Inject(MAT_DIALOG_DATA) public data: any){}

	ngOnInit() {

		this.modalTest = this.data;
	}



	close(){
		this.thisModalRef.close();
	}
}
