import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { NgForm } from '@angular/forms';
import { LibraryService } from '../../../_services/library.service';
import { ProspectQuestionaireComponent } from '../../../prospects/prospect-questionaire/prospect-questionaire.component';
import { DataBusService } from '../../../_services/databus.service';

@Component({
	selector: 'app-update-questionnaire-modal',
	templateUrl: './update-questionnaire-modal.component.html',
	styleUrls: ['./update-questionnaire-modal.component.scss']
})
export class UpdateQuestionnaireModalComponent implements OnInit {


	constructor( private _libraryService:LibraryService,
				 public dialog: MatDialog,
				 public thisModalRef: MatDialogRef<UpdateQuestionnaireModalComponent>
				 ,@Inject(MAT_DIALOG_DATA) public data: any
				, private _dataBus: DataBusService){}

	ngOnInit() {

	}

	close(isRestart){
		if(isRestart)
		{
			let questionaireModal = this.dialog.open(ProspectQuestionaireComponent, {
				data: { name: 'TestData' }
			});
			questionaireModal.afterClosed().subscribe(result => {
			  console.log('The dialog was closed');
			});

		}
		this.thisModalRef.close(isRestart);
	}
}
