import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateQuestionnaireModalComponent } from './update-questionnaire-modal.component';

describe('UpdateQuestionnaireModalComponent', () => {
  let component: UpdateQuestionnaireModalComponent;
  let fixture: ComponentFixture<UpdateQuestionnaireModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateQuestionnaireModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateQuestionnaireModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
