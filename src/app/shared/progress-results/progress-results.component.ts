import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'progress-results',
	templateUrl: './progress-results.component.html',
	styleUrls: ['./progress-results.component.scss']
})
export class ProgressResultsComponent implements OnInit {

	@Input() score:number; 
	@Input() scoreMax:number;

	public scoreArray;
	public scoreMaxArray;

	constructor() {

	}

	ngOnInit() {
		this.scoreArray = Array.from(Array(this.score).keys())
		this.scoreMaxArray = Array.from(Array(this.scoreMax - this.score).keys())
	}

	scoreClassname(){
		if(this.score <= 50){
			return "low";
		}
		else if(this.score > 50 && this.score <= 75){
			return "medium";
		}
		else if(this.score > 75 && this.score < 90){
			return "medium-high";
		}
		else if(this.score > 90){
			return "high";
		}
	}

}
