import { Component, OnInit, Input, ElementRef,ViewChild } from '@angular/core';
import { WebCamComponent } from 'ack-angular-webcam';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DocumentService } from '../../_services/document.service';
import { DataBusService } from '../../_services/databus.service';

import { FullImageModalComponent } from '../modals/full-image-modal/full-image-modal.component';

@Component({
	selector: 'document-add',
	templateUrl: './document-add.component.html',
	styleUrls: ['./document-add.component.scss'],
	host: {'class': 'm-doc-add'}
})
export class DocumentAddComponent implements OnInit {

	@Input() title:string;
	@Input() public set documentType(val: any) {
		this._documentType = val;
		this.loadDocuments();
	  }
	  _documentType:any;
	@ViewChild('myInput') myInputVariable: any;
	public addSection:boolean = false;
	activeProspect:any;
	constructor( private element: ElementRef
				,public dialog: MatDialog
				,private _documentService:DocumentService
				,private _dataBus: DataBusService) { }

	ngOnInit() {
		this._dataBus.ActiveProspect.subscribe(activeProspect => 
			{
			  this.activeProspect = activeProspect.uniqueId;
			});
	}

	uploadDocument(image:any)
	{
		this._documentService.uploadDocument({prospectId:this.activeProspect,documentTypeId:this._documentType.id,image:image}).subscribe((data: any) => {
			if(data.result.status > 0)
			{
				this._documentType.documents[this._documentType.documents.length-1].path= image;
				this._documentType.documents[this._documentType.documents.length-1].isLoaded= true;
				
			}			
	  });  		
	};

	loadDocuments()
	{
		for(var i = 0; i <  this._documentType.documents.length; i++){
				this._documentService.getDocument(this._documentType.documents[i].id).subscribe((data: any) => {
					this.attachDocument(data);
					console.log(this._documentType);
			}); 
		}
	};

	attachDocument(payLoad:any)
	{
		for(var i = 0; i <  this._documentType.documents.length; i++){
			if(this._documentType.documents[i].id == payLoad.id)
			{
				this._documentType.documents[i].path = payLoad.document;
				this._documentType.documents[i].isLoaded = true;
			}
		}

	}

	onFileChange(event)
	{
		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			let file = event.target.files[0];
			reader.readAsDataURL(file);
			reader.onload = () => {
				var image = reader.result;
				this.myInputVariable.nativeElement.value = "";
				this._documentType.documents.push({id:0,path:"",isLoaded :false});
				this.uploadDocument(image)
			}
		}
	}

	openAdd(){
		this.addSection = true;
	}
	closeAdd(){
		this.addSection = false;
	}

	largeImageModal(image:string){
	    let imageModal = this.dialog.open(FullImageModalComponent, {
	    	data: { image: image}
	    });
	    imageModal.afterClosed().subscribe(result => {
	      console.log('The dialog was closed');
	      //this.testResuts = result;
	    });
	}
}
