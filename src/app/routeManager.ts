import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TokenService } from './_services/token.service';

@Injectable()

export class RouteManager implements CanActivate {
    private userService: any;
    public permissions: any;
    private loadedRole: any;
    private roles: any;

    constructor(private tokenService: TokenService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        this.loadedRole = this.tokenService.getRoles();
        let roles = route.data["roles"] as Array<string>;
        let canLoadRoute: boolean = false;
        let rolesLoaded = false;
        if(this.loadedRole)
        {
            for (var role in roles) {
                if (roles[role].indexOf(this.loadedRole) > -1) {
                    canLoadRoute = true;
                    break;
                }
            }
        }
        if (canLoadRoute == false)
            this.router.navigate(['/']);
        return canLoadRoute;
    }
}