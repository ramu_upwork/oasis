import { Component } from '@angular/core';
import {Router, Event, NavigationEnd} from "@angular/router";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	public isMenuVisible: Boolean = true;
	public pagesNoMenu = ['/login', '/login/*','/'];
	public locationPath: string;
	constructor(private _router: Router) {
		this._router.events.subscribe((event:Event) => {
			if(event instanceof NavigationEnd ){
				this.locationPath = event.url;
				for (var i = 0; i < this.pagesNoMenu.length; i++) {
					let path = this.pagesNoMenu[i];
					let isChild = (path.slice(-2) === '/*') ? true : false;
					let valueTest;

					//console.log(path, this.locationPath)
					if(isChild){
						path = path.slice(0, -2);
						valueTest = (this.locationPath.substr(0, path.length) === path) ? true : false;
					}
					else{
						valueTest = (this.locationPath === path) ? true : false
					}

					if(valueTest){
						this.isMenuVisible = false;
						break;
					}
					else{
						this.isMenuVisible = true;
					}	

				}

			}
		});
	}
	
}
