using OasisSalesApi.Domain;
using OasisSalesApi.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace OasisSalesApi.BL
{
  public class QuestionService : ServiceBase, IQuestionService
  {

    public async Task<object> GetVerticals()
    {
      var verticals = await ExecuteActionAsync(async () =>
      {
        return await this.Context.QuestionVerticals.Where(x => x.IsActive).ToListAsync();

      }, "GetVerticals");
      return verticals;
    }

    public async Task<object> GetQuestionaireResultSummary(Guid prospectId)
    {
      var verticals = await ExecuteActionAsync(async () =>
      {
        var resultSet = await
           Task.Run(() =>
           {
             var resultSummary = this.Context.QuestionaireResult
             .Include("QuestionVertical")
             .Include("ResultSets")
             .Include("ResultSets.VerticalSection")
             .Include("ResultSets.QuestionaireResult")
             .Include("ResultSets.Question")
             .Include("ResultSets.Answer")
             .Where(x => x.ProspectId == prospectId)
             .OrderByDescending(x => x.CreatedDate)
             .FirstOrDefault();

             var summary = new List<QuestionResultSummaryVM>();
             foreach (var result in resultSummary.ResultSets.Where(x => x.VerticalSection.IsActive).OrderBy(x => x.VerticalSection.Rank))
             {
               var existingVerticalSection = summary.FirstOrDefault(x => x.VerticalSectionId == result.VerticalSection.Id);
               if (existingVerticalSection != null)
               {
                 existingVerticalSection.TotalScore = existingVerticalSection.TotalScore + result.Answer.Rank;
                 existingVerticalSection.ResultSet.Add(new QuestionaireResultSetVM
                 {
                   AnswerId = result.AnswerId,
                   QuestionId = result.QuestionId,
                   Question = result.Question.Copy,
                   Rank = result.Question.Rank,
                   SelectedAnswer = result.Answer.Copy,
                   Point = result.Answer.Rank

                 });
               }
               else
               {
                 var newSummary = new QuestionResultSummaryVM
                 {
                   VerticalSectionName = result.VerticalSection.Name,
                   VerticalSectionId = result.VerticalSection.Id,
                   TotalScore = result.Answer.Rank,
                   Recommendations = result.VerticalSection.Recommendations.Split('@').ToList()
                 };
                 newSummary.ResultSet.Add(new QuestionaireResultSetVM
                 {
                   AnswerId = result.AnswerId,
                   QuestionId = result.QuestionId,
                   Question = result.Question.Copy,
                   SelectedAnswer = result.Answer.Copy,
                   Point = result.Answer.Rank

                 });
                 summary.Add(newSummary);
               }
             }

             foreach (var item in summary)
             {
               item.TotalScore = (item.TotalScore / 20) * 100;
               item.ResultSet.OrderBy(x => x.Rank);
             }
             return new { resultSummary.Id, summary };
           });
        return resultSet;
      }, "GetQuestionaireResultSummary");
      return verticals;

    }

    public object SaveResult(QuestionResultVM questionResult)
    {
      var verticals = ExecuteAction(() =>
      {
        var questionaireResult = new QuestionaireResult
        {
          ProspectId = questionResult.ProspectId,
          QuestionVerticalId = questionResult.VerticalSectionId,
          CreatedDate = DateTime.Now,
          ResultSets = new List<QuestionaireResultSet>()
        };

        foreach (var section in questionResult.Sections)
        {
          foreach (var answer in section.Answers)
          {
            questionaireResult.ResultSets.Add(new QuestionaireResultSet
            {
              SectionId = section.SectionId,
              AnswerId = answer.AnswerId,
              QuestionId = answer.QuestionId
            });
          }
        }
        this.Context.QuestionaireResult.Add(questionaireResult);
        return this.Context.SaveChanges();
      }, "SaveResult");
      return verticals;
    }

    public object SaveSummaryPdf(SavePdfVM pdf)
    {
      var status = ExecuteAction(() =>
      {
        var resultSet = this.Context.QuestionaireResult.FirstOrDefault(x => x.Id == pdf.Id);
        if (resultSet?.PdfContent != null)
        {
          foreach (var page in resultSet.PdfContent.ToList())
          {
            this.Context.QuestionaireResultPdf.Remove(page);
          }
        }

        if (resultSet == null) return this.Context.SaveChanges();
        {
          foreach (var page in pdf.Pages)
          {
            resultSet.PdfContent.Add(new QuestionaireResultPdf
            {
              PageNumber = page.PageNumber,
              Page = page.Content
            });
          }
        }
        return this.Context.SaveChanges();
      }, "SaveSummaryPdf");
      return status;
    }

  }
}
