using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using OasisSalesApi.Domain.Token;
using OasisSalesApi.ViewModel;

namespace OasisSalesApi.BL
{
  public class AuthenticationService : ServiceBase, IAuthenticationService
  {
    public object ValidateUser(CredentialsVM credentilas)
    {
      var result = ExecuteAction(() =>
      {
        var matchingUser = this.Context.Users
           .FirstOrDefault(x => x.Email == credentilas.UserName.Trim() && x.Password == credentilas.Password.Trim());
        return matchingUser != null
          ? new
          {
            IsAuthenticated = true,
            Token = new Token(matchingUser.Id, matchingUser.FirstName + " " + matchingUser.LastName, "SalesAssociate",
              matchingUser.Email).Encrypt()
          }
          : new
          {
            IsAuthenticated = false,
            Token = ""
          };
      }, "ValidateUser");
      return result;
    }
  }
}
