using OasisSalesApi.Domain;
using OasisSalesApi.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using OasisSalesApi.ViewModel;
using WebSupergoo.ABCpdf10;
using System.Net.Mail;

namespace OasisSalesApi.BL
{
  public class ProspectService : ServiceBase, IProspectService
  {
    public async Task<object> GetProspect(Guid identifier)
    {
      var prospects = await ExecuteActionAsync(async () =>
      {
        var result = await
          Task.Run(() =>
          {
            var healthCheck = this.Context.QuestionaireResult
              .Include("PdfContent")
              .Where(x => x.ProspectId == identifier)
              .OrderByDescending(y => y.CreatedDate).FirstOrDefault();
            return new
            {
              Details = this.Context.Prospects
                .Include("Status")
                .Where(x => x.UniqueId == identifier)
                .Select(x => new
                {
                  x.Id,
                  x.UniqueId,
                  x.OrgUID,
                  x.CMUID,
                  x.ProjectUID,
                  DBA = String.IsNullOrEmpty(x.DBA) ? "N/A" : x.DBA,
                  City = String.IsNullOrEmpty(x.City) ? "N/A" : x.City,
                  PostalCode = String.IsNullOrEmpty(x.PostalCode) ? "N/A" : x.PostalCode,
                  x.EmployeeCount,
                  CurrentPEO = String.IsNullOrEmpty(x.CurrentPEO) ? "N/A" : x.CurrentPEO,
                  BenefitsCarrier = String.IsNullOrEmpty(x.BenefitsCarrier) ? "N/A" : x.BenefitsCarrier,
                  WCCarrier = String.IsNullOrEmpty(x.WCCarrier) ? "N/A" : x.WCCarrier,
                  Address = (String.IsNullOrEmpty(x.Address1) ? "N/A" : x.Address1)+ (String.IsNullOrEmpty(x.Address2) ? "" : x.Address2),
                  CompanyName = String.IsNullOrEmpty(x.Name) ? "N/A" : x.Name,
                  Fax = String.IsNullOrEmpty(x.Fax) ? "000-000-000" : x.Fax,
                  Phone = String.IsNullOrEmpty(x.Phone) ? "000-000-000" : x.Phone,
                  Status = x.Status.Name,
                  Website = String.IsNullOrEmpty(x.Website) ? "N/A" : x.Website,
                  x.IsSummaryCreated,
                  x.SummaryFile,
                  x.CreatedDate
                })
                .FirstOrDefault(),
              HealthCheck = new
              {
                IsCompleted = healthCheck != null,
                IsPdfCreated = healthCheck?.PdfContent.Count > 0,
                ResultId = healthCheck?.Id
              }
            };
          });
        return result;

      }, "GetProspect");
      return prospects;
    }

    public async Task<object> GetProspectList(int statusFilter, string search, string userId)
    {
      var prospects = await ExecuteActionAsync(async () =>
      {
        if (statusFilter != 0)
        {
          var results = await
             Task.Run(() =>
             {
               return this.Context.Prospects
                .Include("Status")
                .Where(x => x.StatusId == statusFilter && x.AssociateEmail == userId.Trim() && x.Name.Contains(search))
                .Select(x => new
                {
                  x.Id,
                  x.UniqueId,
                  x.OrgUID,
                  x.CMUID,
                  x.ProjectUID,
                  DBA = String.IsNullOrEmpty(x.DBA) ? "N/A" : x.DBA,
                  City = String.IsNullOrEmpty(x.City) ? "N/A" : x.City,
                  PostalCode = String.IsNullOrEmpty(x.PostalCode) ? "N/A" : x.PostalCode,
                  x.EmployeeCount,
                  CurrentPEO = String.IsNullOrEmpty(x.CurrentPEO) ? "N/A" : x.CurrentPEO,
                  BenefitsCarrier = String.IsNullOrEmpty(x.BenefitsCarrier) ? "N/A" : x.BenefitsCarrier,
                  WCCarrier = String.IsNullOrEmpty(x.WCCarrier) ? "N/A" : x.WCCarrier,
                  Address = (String.IsNullOrEmpty(x.Address1) ? "N/A" : x.Address1) + (String.IsNullOrEmpty(x.Address2) ? "" : x.Address2),
                  CompanyName = String.IsNullOrEmpty(x.Name) ? "N/A" : x.Name,
                  Fax = String.IsNullOrEmpty(x.Fax) ? "000-000-000" : x.Fax,
                  Phone = String.IsNullOrEmpty(x.Phone) ? "000-000-000" : x.Phone,
                  Status = x.Status.Name,
                  Website = String.IsNullOrEmpty(x.Website) ? "N/A" : x.Website,
                  x.IsSummaryCreated,
                  x.SummaryFile,
                  x.CreatedDate
                })
                .OrderBy(x => x.CreatedDate)
                .ToList();
             });
          return results;
        }
        else
        {
          var results = await
             Task.Run(() =>
             {
               return this.Context.Prospects
                .Include("Status")
                 .Where(x => x.AssociateEmail == userId.Trim() && x.Name.Contains(search))
                .Select(x => new
                {
                  x.Id,
                  x.UniqueId,
                  x.OrgUID,
                  x.CMUID,
                  x.ProjectUID,
                  DBA = String.IsNullOrEmpty(x.DBA) ? "N/A" : x.DBA,
                  City = String.IsNullOrEmpty(x.City) ? "N/A" : x.City,
                  PostalCode = String.IsNullOrEmpty(x.PostalCode) ? "N/A" : x.PostalCode,
                  x.EmployeeCount,
                  CurrentPEO = String.IsNullOrEmpty(x.CurrentPEO) ? "N/A" : x.CurrentPEO,
                  BenefitsCarrier = String.IsNullOrEmpty(x.BenefitsCarrier) ? "N/A" : x.BenefitsCarrier,
                  WCCarrier = String.IsNullOrEmpty(x.WCCarrier) ? "N/A" : x.WCCarrier,
                  Address = (String.IsNullOrEmpty(x.Address1) ? "N/A" : x.Address1) + (String.IsNullOrEmpty(x.Address2) ? "" : x.Address2),
                  CompanyName = String.IsNullOrEmpty(x.Name) ? "N/A" : x.Name,
                  Fax = String.IsNullOrEmpty(x.Fax) ? "000-000-000" : x.Fax,
                  Phone = String.IsNullOrEmpty(x.Phone) ? "000-000-000" : x.Phone,
                  Status = x.Status.Name,
                  Website = String.IsNullOrEmpty(x.Website) ? "N/A" : x.Website,
                  x.IsSummaryCreated,
                  x.SummaryFile,
                  x.CreatedDate
                })
                .OrderBy(x => x.CreatedDate)
                .ToList();
             });
          return results;
        }
      }, "GetProspectList");
      return prospects;
    }

    public async Task<object> GetProspectDocuments(Guid identifier)
    {
      var documents = await ExecuteActionAsync(async () =>
      {
        var resultSet = await
        Task.Run(() =>
        {
          var types = this.Context.DocumentTypes
            .Include("SubTypes")
            .Include("SubTypes.Documents")
            .Where(x => x.IsActive)
            .OrderBy(x => x.Rank)
            .Select(x => new
            {
              x.Id,
              x.Name,
              SubTypes = x.SubTypes
                .Where(y => y.IsActive)
                .OrderBy(y => y.Rank)
                .Select(y => new
                {
                  y.Name,
                  y.IsRequired,
                  y.Id,
                  Documents = y.Documents
                    .Where(d => d.Prospect.UniqueId == identifier && d.IsActive)
                    .OrderBy(d => d.Id)
                    .Select(d => new
                    {
                      d.Id,
                      path = "",
                      isLoaded = false
                    })
                })
            })
            .ToList();
          return types;
        });

        return resultSet;
      }, "GetProspectDocuments");
      return documents;
    }

    public async Task<object> GetProspectHealthSummary(Guid identifier)
    {
      var healthSummary = await ExecuteActionAsync(async () =>
      {
        var resultSet = await
          Task.Run(() =>
          {
            var resultSummary = this.Context.QuestionaireResult
            .Include("QuestionVertical")
            .Include("ResultSets")
            .Include("ResultSets.VerticalSection")
            .Include("ResultSets.QuestionaireResult")
            .Include("ResultSets.Question")
            .Include("ResultSets.Answer")
            .Where(x => x.ProspectId == identifier)
            .OrderByDescending(x => x.CreatedDate)
            .FirstOrDefault();

            var summary = new List<QuestionResultSummaryVM>();
            foreach (var result in resultSummary.ResultSets.Where(x => x.VerticalSection.IsActive).OrderBy(x => x.VerticalSection.Rank))
            {
              var existingVerticalSection = summary.FirstOrDefault(x => x.VerticalSectionId == result.VerticalSection.Id);
              if (existingVerticalSection != null)
              {
                existingVerticalSection.TotalScore = existingVerticalSection.TotalScore + result.Answer.Rank;
                existingVerticalSection.ResultSet.Add(new QuestionaireResultSetVM
                {
                  AnswerId = result.AnswerId,
                  QuestionId = result.QuestionId,
                  Question = result.Question.Copy,
                  Rank = result.Question.Rank,
                  SelectedAnswer = result.Answer.Copy,
                  Point = result.Answer.Rank

                });
              }
              else
              {
                var newSummary = new QuestionResultSummaryVM
                {
                  VerticalSectionName = result.VerticalSection.Name,
                  VerticalSectionId = result.VerticalSection.Id,
                  TotalScore = result.Answer.Rank
                };
                newSummary.ResultSet.Add(new QuestionaireResultSetVM
                {
                  AnswerId = result.AnswerId,
                  QuestionId = result.QuestionId,
                  Question = result.Question.Copy,
                  SelectedAnswer = result.Answer.Copy,
                  Point = result.Answer.Rank

                });
                summary.Add(newSummary);
              }
            }

            foreach (var item in summary)
            {
              item.TotalScore = (item.TotalScore / 20) * 100;
              item.ResultSet.OrderBy(x => x.Rank);
            }

            return new { resultSummary.Id, summary };
          });
        return resultSet;
      }, "GetProspectHealthSummary");
      return healthSummary;
    }

    public object GenerateSummary(Guid identifier,string salesPerson)
    {
      var prospects = ExecuteAction(() =>
      {
        var prospectInfo = GetCompletePropsectDetails(identifier);
        var coverPages = GetProspectSummaryCoverPages();
        var researchSummary = GetProspectResearchPdf(identifier);

        var pdfFrontPage = ConvertHtmlToPdf(BuildPdfFromHtml(coverPages[0].Template, new
        {
          CompanyName = prospectInfo.Name,
          PECName = salesPerson,
          PECPhone = prospectInfo.Phone,
          PECEmail = prospectInfo.AssociateEmail
        }));

        var pdfIntroPage = ConvertHtmlToPdf(coverPages[1].Template);

        var summaryDoc = new Doc();
        summaryDoc.Append(pdfFrontPage);
        summaryDoc.Append(pdfIntroPage);

        foreach (var researchDoc in researchSummary.Where(x => x.ResearchDocument != null))
        {
          //removed code for appending cover page for each section
          //var researchSectionCover = ConvertHtmlToPdf(researchDoc.CoverPageTemplate);

          var researchPdfArray = OasisUtility.ParseBase64ToArray(researchDoc.ResearchDocument.Pdf);
          var researchPdf = new Doc();
          researchPdf.Read(researchPdfArray);

          //removed code for appending cover page for each section
          //researchSectionCover.Append(researchPdf);
          //summaryDoc.Append(researchSectionCover);

          summaryDoc.Append(researchPdf);
        }

        prospectInfo.IsSummaryCreated = true;
        prospectInfo.SummaryCreatedDate = DateTime.Now;
        var fileName = Guid.NewGuid();
        prospectInfo.SummaryFile = fileName.ToString();
        this.Context.SaveChanges();
        summaryDoc.Save(HttpContext.Current.Server.MapPath("~/ProspectDocuments/SummaryPdf/" + fileName + ".pdf"));
        return new { prospectInfo.IsSummaryCreated, fileName };
      }, "GenerateSummary");
      return prospects;
    }

    public string ViewHealthSummary(Guid identifier)
    {
      var html = ExecuteAction(() =>
      {
        var result = this.Context.QuestionaireResult
          .Include("PdfContent")
          .Where(x => x.ProspectId == identifier)
          .OrderByDescending(x => x.CreatedDate)
          .Select(x => new
          {
            x.Id,
            Pages = x.PdfContent.OrderBy(y => y.PageNumber).Select(z => new { z.Page })
          })
          .FirstOrDefault();
        var pages = result?.Pages.Select(page => page.Page).ToList();
        return pages != null && pages.Count > 0
          ? "<!DOCTYPE html PUBLIC \" -//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" +
            "<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><meta http - equiv = \"Content-Type\" content = \"text/html; charset=utf-8\"/><title> Sales Application </title></head>" +
            "<body>" +
            "<div style=\"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "<img src =\"" + pages[0] + "\" alt = \"\" style = \"display: block;width: 100%\"> " +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\">" +
            "<img src =\"" + pages[1] + "\" alt = \"\"  style = \"display: block;width: 100%\">" +
            "<img src =\"" + pages[2] + "\" alt = \"\"  style = \"display: block;width: 100%\">" +
            "<img src =\"" + pages[3] + "\" alt = \"\"  style = \"display: block;width: 100%\">" +
            "<img src =\"" + pages[4] + "\" alt = \"\"  style = \"display: block;width: 100%\">" +
            "<img src =\"" + pages[5] + "\" alt = \"\"  style = \"display: block;width: 100%\">" +
            "</div></body></html>"
          : "<!DOCTYPE html PUBLIC \" -//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" +
            "<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><meta http - equiv = \"Content-Type\" content = \"text/html; charset=utf-8\"/><title> Sales Application </title></head>" +
            "<body>" +
            "<div style=\"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\" >" +
            "</div>" +
            "<div style = \"width: 900px; padding: 20px; margin - right: auto; margin - bottom: 50px; margin - left: auto;\">" +
            "</div></body></html>";
      }, "ViewHealthSummary");
      return html;
    }

    public async Task<object> SendZipFile(SendZipFileVM zipFileEmail)
    {
      var status = await ExecuteActionAsync(async () =>
      {
        var mailStatus = await
          Task.Run(() =>
          {
            var message = new MailMessage();
            message.To.Add(new MailAddress(zipFileEmail.ReceipientEmail, zipFileEmail.ReceipientEmail));
            message.Subject = "Oasis Sales : Documents";
            message.From = new MailAddress("donot_reply@oasis.com", "donot_reply@oasis.com");
            message.IsBodyHtml = true;
            var attachmentPath = System.Web.Hosting.HostingEnvironment.MapPath(
                  "~/WhitePapers/Documents.zip");
            var attachment =
              new Attachment(attachmentPath);
            message.Attachments.Add(attachment);
            return this.SendMail(message);
          });
        return mailStatus;
      }, "SendZipFile");
      return status;
    }

    Prospect GetCompletePropsectDetails(Guid identifier)
    {
      return this.Context.Prospects
        .Include("Status").FirstOrDefault(x => x.UniqueId == identifier);
    }

    List<ProspectSummaryTemplate> GetProspectSummaryCoverPages()
    {
      return this.Context.ProspectSummaryTemplates
        .Where(x => x.IsActive == true)
        .OrderBy(x => x.PageNumber)
        .ToList();
    }

    List<ResearchDocumentDetailsVM> GetProspectResearchPdf(Guid identifier)
    {
      var types = this.Context.Research
        .Include("Documents")
        .Include("Documents.Prospect")
        .OrderBy(x => x.Rank)
        .Select(x => new ResearchDocumentDetailsVM
        {
          Id = x.Id,
          Name = x.Name,
          CoverPageTemplate = x.CoverPageTemplate,
          ResearchDocument = x.Documents
            .Where(y => y.Prospect.UniqueId == identifier)
            .OrderByDescending(y => y.Id)
            .Select(y => new ResearchDocumentVM
            {
              UniqueId = y.UniqueId,
              Pdf = y.FileContent
            })
            .FirstOrDefault()
        })
        .ToList();
      return types;
    }

   
  }
}
