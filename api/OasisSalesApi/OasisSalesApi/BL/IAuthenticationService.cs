using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using OasisSalesApi.ViewModel;

namespace OasisSalesApi.BL
{
  public interface IAuthenticationService
  {
    object ValidateUser(CredentialsVM credentilas);
  }
}
