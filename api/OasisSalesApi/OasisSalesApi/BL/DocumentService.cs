using OasisSalesApi.Domain;
using OasisSalesApi.ViewModel;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OasisSalesApi.BL
{
  public class DocumentService : ServiceBase, IDocumentService
  {

    public async Task<string> GetDocument(int id)
    {
      var document = await ExecuteActionAsync(async () =>
      {
        var documentContent = await
          Task.Run(() =>
          {
            return this.Context.Documents
               .FirstOrDefault(x => x.Id == id)
               ?.FileContent;
          });

        return documentContent;
      }, "GetDocument");
      return document;
    }

    public async Task<object> GetDocumentTypes()
    {
      var documentTypes = await ExecuteActionAsync(async () =>
       {
         var resultSet = await
           Task.Run(() =>
           {
             return this.Context.DocumentTypes
               .Include("SubTypes")
               .Include("SubTypes.Documents")
               .Where(x => x.IsActive)
               .OrderBy(x => x.Rank)
               .Select(x => new
               {
                 x.Id,
                 x.Name,
                 SubTypes = x.SubTypes
                   .Where(y => y.IsActive)
                   .OrderBy(y => y.Rank)
                   .Select(y => new
                   {
                     y.Name,
                     y.IsRequired,
                     y.Id
                   })
               })
               .ToList();
           });

         return resultSet;
       }, "GetDocument");
      return documentTypes;

    }

    public object UploadDocument(DocumentUploadVM payload)
    {
      var document = ExecuteAction(() =>
      {
        var prospect = this.Context.Prospects.FirstOrDefault(x => x.UniqueId == payload.ProspectId);
        var newDocument = new Document
        {
          IsActive = true,
          ProspectId = prospect.Id,
          FileContent = payload.Image,
          Rank = 1,
          TypeId = payload.DocumentTypeId,
          UniqueId = Guid.NewGuid()
        };
        var savedDocument = this.Context.Documents.Add(newDocument);
        var result = this.Context.SaveChanges();
        return new { Status = result, DocumentId = savedDocument.Id };
      }, "UploadDocument");
      return document;
    }
  }
}
