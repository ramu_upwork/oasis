using OasisSalesApi.Domain;
using OasisSalesApi.ViewModel;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OasisSalesApi.BL
{
  public class ResearchService : ServiceBase, IResearchService
  {
    public string DownloadDocument(Guid identifeir)
    {
      var pdf = ExecuteAction(() =>
      {
        var content = this.Context.ResearchDocuments
          .FirstOrDefault(x => x.UniqueId == identifeir)
          ?.FileContent;
        string[] words = content.Split(',');
        return words.Length == 2 ? words[1] : string.Empty;
      }, "DownloadDocument");
      return pdf;
    }

    public async Task<object> GetProspectResearchDocuments(Guid identifier)
    {
      var researchs = await ExecuteActionAsync(async () =>
      {
        var resultSet = await
          Task.Run(() =>
          {
            var types = this.Context.Research
              .Include("Documents")
              .Include("Documents.Prospect")
              .OrderBy(x => x.Rank)
              .Select(x => new
              {
                x.Id,
                x.Name,
                x.Description,
                x.Url,
                IsSelected = false,
                IsUploading = false,
                Document = x.Documents
                  .Where(y => y.Prospect.UniqueId == identifier)
                  .OrderByDescending(y => y.Id)
                  .Select(y => new
                  {
                    y.UniqueId
                  })
                  .FirstOrDefault()
              })
              .ToList();
            return types;
          });

        return resultSet;
      }, "GetProspectResearchDocuments");
      return researchs;
    }

    public object UploadDocument(ResearchDocumentUploadVM payload)
    {
      var document = ExecuteAction(() =>
      {
        var prospect = this.Context.Prospects.FirstOrDefault(x => x.UniqueId == payload.ProspectId);
        var newDocument = new ResearchDocument
        {
          ProspectId = prospect.Id,
          ResearchId = payload.ResearchId,
          FileContent = payload.Document,
          UniqueId = Guid.NewGuid(),
          CreatedDate = DateTime.Now,
          FileType = string.Empty,
        };
        var savedDocument = this.Context.ResearchDocuments.Add(newDocument);
        var result = this.Context.SaveChanges();
        return new { Status = result, DocumentId = savedDocument.UniqueId };
      }, "UploadDocument");
      return document;
    }
  }
}
