using OasisSalesApi.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OasisSalesApi.BL
{
  public interface ILibraryService
  {
    Task<Object> SendWhitePaper(SendWhitePaperVM whitePaperEmail);
  }
}
