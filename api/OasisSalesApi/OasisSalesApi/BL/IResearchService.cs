using OasisSalesApi.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OasisSalesApi.BL
{
  public interface IResearchService
  {
    Task<Object> GetProspectResearchDocuments(Guid identifier);

    object UploadDocument(ResearchDocumentUploadVM payload);

    string DownloadDocument(Guid identifeir);

  }
}
