using OasisSalesApi.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSupergoo.ABCpdf10;

namespace OasisSalesApi.BL
{
  public interface IProspectService
  {

    Task<Object> GetProspectList(int statusFilter, string search, string userId);

    Task<Object> GetProspect(Guid identifier);

    Task<Object> GetProspectDocuments(Guid identifier);

    Task<Object> GetProspectHealthSummary(Guid prospectId);

    string ViewHealthSummary(Guid prospectId);

    object GenerateSummary(Guid identifier, string salesPerson);

    Task<Object> SendZipFile(SendZipFileVM zipFileEmail);
  }
}
