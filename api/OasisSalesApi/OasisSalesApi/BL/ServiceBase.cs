using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using OasisSalesApi.Domain;
using WebSupergoo.ABCpdf10;

namespace OasisSalesApi.BL
{
  public class ServiceBase
  {

    protected readonly OasisContext Context;

    public ServiceBase()
    {
      Context = new OasisContext();

    }

    public async Task<T> ExecuteActionAsync<T>(Func<Task<T>> action, string method)
    {
      try
      {
        return await action.Invoke();
      }
      catch (Exception ex)
      {
        // ExceptionHandler.HandleException(new ServiceLayerException(method, ex));
        return default(T);
      }
    }

    public T ExecuteAction<T>(Func<T> action, string method)
    {
      try
      {
        return action.Invoke();
      }
      catch (Exception ex)
      {
        // ExceptionHandler.HandleException(new ServiceLayerException(method, ex));
        return default(T);
      }
    }

    protected bool SendMail(MailMessage message)
    {
      try
      {
        SmtpClient smtp = new SmtpClient("hf7h-5q8k.accessdomain.com", 587)
        {
          Credentials = new NetworkCredential("app@sgcagency.com", "P@ssw0rd5gc"),
          EnableSsl = true
        };
        smtp.Send(message);
        return true;
      }
      catch (Exception ex)
      {
        return false;
      }

    }

    protected string BuildPdfFromHtml(string html, object model)
    {
      return Engine.Razor.RunCompile(html, Guid.NewGuid().ToString(), null, model);
    }

    protected Doc ConvertHtmlToPdf(string html, string header = null, string footer = null, bool isPageNumberInFooter = false)
    {
      // Create ABCpdf Doc object
      var doc = new Doc();

      if (header == null && footer == null)
        doc.Rect.Inset(20, 20);
      else
        doc.Rect.String = "0 70 610 760"; /*padding from left, padding from bottom, width from left, height from bottom*/

      // Add html to Doc                
      int theId = doc.AddImageHtml(html);

      // Loop through document to create multi-page PDF
      while (true)
      {
        if (!doc.Chainable(theId))
          break;
        doc.Page = doc.AddPage();
        theId = doc.AddImageToChain(theId);
      }
      var count = doc.PageCount;

      /*****************Footer area******************/
      if (footer != null)
      {
        var newfooter = "";
        doc.Rect.String = "40 20 580 50";
        for (int i = 1; i <= count; i++)
        {

          doc.PageNumber = i;
          if (isPageNumberInFooter)
          {
            newfooter = footer.Replace("PageNumber", "Page " + i.ToString() + " of " + count.ToString());
            int id = doc.AddImageHtml(newfooter);

            while (true)
            {
              if (!doc.Chainable(id))
                break;
              id = doc.AddImageToChain(id);
            }
          }
          else
            doc.AddText(footer);
        }
      }
      // Flatten the PDF
      for (int i = 1; i <= doc.PageCount; i++)
      {
        doc.PageNumber = i;
        doc.Flatten();
      }

      //var pdf = doc.GetData();
      //doc.Clear();
      return doc;
    }

  }
}
