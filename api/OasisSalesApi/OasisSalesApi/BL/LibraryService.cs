using OasisSalesApi.ViewModel;
using System.Net.Mail;
using System.Threading.Tasks;

namespace OasisSalesApi.BL
{
  public class LibraryService : ServiceBase, ILibraryService
  {
    public async Task<object> SendWhitePaper(SendWhitePaperVM whitePaperEmail)
    {
      var status = await ExecuteActionAsync(async () =>
      {
        var mailStatus = await
          Task.Run(() =>
          {
            var message = new MailMessage();
            message.To.Add(new MailAddress(whitePaperEmail.ReceipientEmail, whitePaperEmail.ReceipientEmail));
            message.Subject = "Oasis Sales : White Paper";
            message.From = new MailAddress("donot_reply@oasis.com", "donot_reply@oasis.com");
            message.IsBodyHtml = true;
            var attachmentPath = System.Web.Hosting.HostingEnvironment.MapPath(
                  "~/WhitePapers/PayScale-5 Steps-Comp-Plan-Guide.pdf");
            var attachment =
              new Attachment(attachmentPath);
            message.Attachments.Add(attachment);
            return this.SendMail(message);
          });
        return mailStatus;
      }, "SendWhitePaper");
      return status;
    }
  }
}
