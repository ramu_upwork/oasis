using OasisSalesApi.Domain;
using OasisSalesApi.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OasisSalesApi.BL
{
  public interface IQuestionService
  {
    Task<object> GetVerticals();

    object SaveResult(QuestionResultVM results);

    object SaveSummaryPdf(SavePdfVM pdf);

    Task<object> GetQuestionaireResultSummary(Guid prospectId);
  }
}
