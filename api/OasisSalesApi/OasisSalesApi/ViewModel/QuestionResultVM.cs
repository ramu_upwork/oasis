using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OasisSalesApi.ViewModel
{
  public class QuestionResultVM
  {

    public Guid ProspectId { get; set; }
    public int VerticalSectionId { get; set; }
    public List<QuestionResultAnswersVM> Sections { get; set; }

  }

  public class SavePdfVM
  {
    public SavePdfVM()
    {
      Pages = new List<PdfPageVM>();
    }
    public int Id { get; set; }
    public List<PdfPageVM> Pages { get; set; }
  }

  public class PdfPageVM
  {
    public int PageNumber { get; set; }
    public string Content { get; set; }
  }

  public class QuestionResultAnswersVM
  {
    public int SectionId { get; set; }
    public List<AnswersVM> Answers { get; set; }
  }

  public class AnswersVM
  {
    public int QuestionId { get; set; }
    public int AnswerId { get; set; }
  }

  public class QuestionaireResultSetVM
  {
    public int QuestionId { get; set; }
    public int AnswerId { get; set; }
    public string Question { get; set; }
    public int Rank { get; set; }
    public string SelectedAnswer { get; set; }
    public int Point { get; set; }
  }

  public class QuestionResultSummaryVM
  {
    public QuestionResultSummaryVM()
    {
      ResultSet = new List<QuestionaireResultSetVM>();
      Recommendations = new List<String>();
    }
    public int VerticalSectionId { get; set; }
    public string VerticalSectionName { get; set; }
    public decimal TotalScore { get; set; }
    public List<QuestionaireResultSetVM> ResultSet { get; set; }
    public List<string> Recommendations { get; set; }

  }
}
