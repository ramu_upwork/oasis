using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OasisSalesApi.ViewModel
{
  public class SendZipFileVM
  {
    public string ReceipientEmail { get; set; }
  }
}
