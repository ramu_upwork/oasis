using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OasisSalesApi.ViewModel
{
  public class ResearchDocumentUploadVM
  {
    public Guid ProspectId { get; set; }
    public int ResearchId { get; set; }
    public string Document { get; set; }
  }

  public class ResearchDocumentDetailsVM
  {
    
    public int Id { get; set; }
    public string Name { get; set; }
    public string CoverPageTemplate { get; set; }
    public ResearchDocumentVM ResearchDocument { get; set; }
  }

  public class ResearchDocumentVM
  {
    public Guid UniqueId { get; set; }
    public string Pdf { get; set; }
  }

}
