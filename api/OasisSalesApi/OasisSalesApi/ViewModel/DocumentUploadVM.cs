using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OasisSalesApi.ViewModel
{
  public class DocumentUploadVM
  {
    public Guid ProspectId { get; set; }
    public int DocumentTypeId { get; set; }
    public string Image { get; set; }
  }
}
