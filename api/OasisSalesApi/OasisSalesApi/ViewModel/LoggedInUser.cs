using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OasisSalesApi.ViewModel
{
  public class LoggedInUserVM
  {
    public int Id { get; set; }
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
  }

  public class CredentialsVM
  {
    public string UserName { get; set; }
    public string Password { get; set; }
  }
}
