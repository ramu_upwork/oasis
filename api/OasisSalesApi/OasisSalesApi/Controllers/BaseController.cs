using OasisSalesApi.BL;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using OasisSalesApi.Domain.Token;
using WebSupergoo.ABCpdf10;

namespace OasisSalesApi.Controllers
{
  public class BaseController : ApiController
  {
    protected Token LoggedInUser { get; set; }

    /// <summary>
    /// Gets the instance of the application service class used by the WebAPI controller.This must be overridden by all the
    /// class which inherits this classes
    /// </summary>
    protected virtual ServiceBase Service => null;

    /// <summary>
    /// The asynchronous function which will be executed before each WebAPI requests.
    /// </summary>
    /// <param name="controllerContext">The controller context.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>result of the operation.</returns>
    public override Task<HttpResponseMessage> ExecuteAsync(
        HttpControllerContext controllerContext,
        CancellationToken cancellationToken)
    {
      try
      {
        //validate the application service instance.
        if (this.Service != null)
        {
          if (controllerContext.Request.Headers.Contains("X-Token"))
          {
            string encryptedToken = controllerContext.Request.Headers.GetValues("X-Token").First();
            if (!String.IsNullOrEmpty(encryptedToken))
            {
               this.LoggedInUser = Token.Decrypt(encryptedToken);
            }
          }
        }
        return base.ExecuteAsync(controllerContext, cancellationToken)
           .ContinueWith(task => task)
               .Unwrap();
      }
      catch (Exception e)
      {
        throw e;
      }
    }


    protected byte[] ConvertHtmlToPdf(string html, string header = null, string footer = null, bool isPageNumberInFooter = false)
    {
      // Create ABCpdf Doc object
      var doc = new Doc();

      if (header == null && footer == null)
        doc.Rect.Inset(20, 20);
      else
        doc.Rect.String = "0 70 610 760"; /*padding from left, padding from bottom, width from left, height from bottom*/

      // Add html to Doc                
      int theId = doc.AddImageHtml(html);

      // Loop through document to create multi-page PDF
      while (true)
      {
        if (!doc.Chainable(theId))
          break;
        doc.Page = doc.AddPage();
        theId = doc.AddImageToChain(theId);
      }
      var count = doc.PageCount;

      /*****************Footer area******************/
      if (footer != null)
      {
        var newfooter = "";
        doc.Rect.String = "40 20 580 50";
        for (int i = 1; i <= count; i++)
        {

          doc.PageNumber = i;
          if (isPageNumberInFooter)
          {
            newfooter = footer.Replace("PageNumber", "Page " + i.ToString() + " of " + count.ToString());
            int id = doc.AddImageHtml(newfooter);

            while (true)
            {
              if (!doc.Chainable(id))
                break;
              id = doc.AddImageToChain(id);
            }
          }
          else
            doc.AddText(footer);
        }
      }

      for (int i = 1; i <= doc.PageCount; i++)
      {
        doc.PageNumber = i;
        doc.Flatten();
      }

      var pdf = doc.GetData();
      doc.Clear();
      // Get PDF as byte array. Couls also use .Save() to save to disk
      return pdf;
    }
  }
}
