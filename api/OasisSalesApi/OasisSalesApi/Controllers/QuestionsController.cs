using OasisSalesApi.BL;
using OasisSalesApi.Domain;
using OasisSalesApi.ViewModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using WebSupergoo.ABCpdf10;

namespace OasisSalesApi.Controllers
{
  public class QuestionsController : BaseController
  {

    private readonly BL.IQuestionService _service;

    protected override ServiceBase Service => (ServiceBase)this._service;

    public QuestionsController()
            : this(new QuestionService())
    {

    }

    public QuestionsController(IQuestionService service)
    {
      this._service = service;
    }


    [HttpGet]
    public async Task<object> GetVerticals()
    {
      return await this._service.GetVerticals();
    }

    [HttpGet]
    public async Task<object> GetQuestionaireResultSummary(Guid prospectId)
    {
      return await this._service.GetQuestionaireResultSummary(prospectId);
    }


    [HttpPost]
    public IHttpActionResult SaveResult(QuestionResultVM results)
    {
      return Ok(this._service.SaveResult(results));
    }

    [HttpPost]
    public IHttpActionResult SaveSummaryPdf(SavePdfVM pdf)
    {
      return Ok(this._service.SaveSummaryPdf(pdf));
    }

    //Need to re write the below peice of code for retrieving questions
    private OasisContext db = new OasisContext();

    [HttpGet]
    public IHttpActionResult GetQuestions(int sectionId)
    {
      try
      {

        var verticalSections = db.VerticalSections.Where(x => x.IsActive && x.QuestionVerticalId == sectionId).ToList().OrderBy(y => y.Rank);
        var sections = new List<object>();
        int sectionNumber = 0;
        int questionNumber = 0;
        foreach (var vs in verticalSections)
        {

          var vsQuestion = db.Questions.Where(x => x.IsActive && x.VerticalSectionId == vs.Id).ToList().OrderBy(y => y.Rank);
          var quesObject = new List<object>();
          foreach (var qs in vsQuestion)
          {

            var ansObject = new List<object>();
            var qsAnswer = db.Answers.Where(x => x.IsActive && x.QuestionId == qs.Id).ToList().OrderBy(y => y.Rank);
            foreach (var ans in qsAnswer)
            {
              ansObject.Add(new
              {
                AnswerId = ans.Id,
                Answer = ans.Copy,
                Point = ans.Rank,
                AnswerRank = ans.Rank,
                IsSelected = "false"
              });
            }

            quesObject.Add(new
            {
              Number = ++questionNumber,
              QuestionId = qs.Id,
              Question = qs.Copy,
              QuestionRank = qs.Rank,
              Answers = ansObject
            });

          }
          sections.Add(new
          {
            Number = ++sectionNumber,
            SectionName = vs.Name,
            SectionId = vs.Id,
            SectionRank = vs.Rank,
            Question = quesObject
          });
        }
        return Ok(new { Sections = sections, QuestiionsCount = questionNumber });
      }
      catch (Exception ex)
      {
        return Ok(ex.Message + "Inner Exception" + ex.InnerException?.Message);
      }
    }
  }
}
