using OasisSalesApi.BL;
using OasisSalesApi.Utility;
using OasisSalesApi.ViewModel;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace OasisSalesApi.Controllers
{
  public class ResearchController : BaseController
  {
    private readonly BL.IResearchService _service;

    protected override ServiceBase Service => (ServiceBase)this._service;

    public ResearchController()
            : this(new ResearchService())
    {

    }

    public ResearchController(IResearchService service)
    {
      this._service = service;
    }

    [HttpGet]
    public async Task<object> GetProspectResearchDocuments(Guid identifier)
    {
      return await _service.GetProspectResearchDocuments(identifier);
    }

    [HttpPost]
    public object UploadDocument(ResearchDocumentUploadVM payload)
    {
      return new { Result = _service.UploadDocument(payload) };
    }

    [HttpGet]
    public HttpResponseMessage DownLoadDocument(Guid identifier)
    {
      try
      {
        var fileString = _service.DownloadDocument(identifier);
        var fileType = OasisUtility.GetFileExtension(fileString);
        var fileContent = Convert.FromBase64String(fileString);
        var response = new HttpResponseMessage(HttpStatusCode.OK);
        Stream stream = new MemoryStream(fileContent);
        response.Content = new StreamContent(stream);
        response.Content.Headers.ContentType = new MediaTypeHeaderValue(fileType.MediaTypeHeaderValue);
        response.Content.Headers.ContentDisposition =
          new ContentDispositionHeaderValue("inline") { FileName = fileType.Name };
        return response;
      }
      catch (Exception ex)
      {
        return this.Request.CreateResponse(HttpStatusCode.OK, "File Not Found");
      }

    }

  }
}
