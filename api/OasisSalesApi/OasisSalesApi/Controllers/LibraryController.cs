using OasisSalesApi.BL;
using OasisSalesApi.Domain;
using OasisSalesApi.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace OasisSalesApi.Controllers
{
  public class LibraryController : BaseController
  {

    private readonly BL.ILibraryService _service;

    protected override ServiceBase Service => (ServiceBase)this._service;

    public LibraryController()
            : this(new LibraryService())
    {

    }

    public LibraryController(ILibraryService service)
    {
      this._service = service;
    }

    [HttpPost]
    public async Task<object> SendWhitePaper(SendWhitePaperVM whitePaperEmail)
    {
      return await _service.SendWhitePaper(whitePaperEmail);
    }

    [HttpGet]
    public async Task<object> GetLibrary(int typeFilter = 0, string search = "")
    {
      var libraryItems = new List<Library>
      {
        new Library
        {
          Id = 1,
          Title = "PayScale-5 Steps-Comp-Plan-Guide",
          TypeId = 1,
          Url = "assets/pdf/PayScale-5 Steps-Comp-Plan-Guide.pdf",
          Preview = "assets/pdf/preview/PayScale-5 Steps-Comp-Plan-Guide-1.jpg",
          Description =
            "How smart is your company? Smart companies have up-to-date compensation plans that support their business objectives and ensure their long-term success."
        },
        new Library
        {
          Id = 2,
          Title = "2017-compensation-best-practices-report",
          TypeId = 2,
          Url = "assets/pdf/2018-Payscale-compensation-best-practices-report.pdf",
          Preview = "assets/pdf/preview/2018-Payscale-compensation-best-practices-report-01.jpg",
          Description =
            "A sarcastic aside referencing a pay increase so small, it equates to about one latte per paycheck."
        },
        new Library
        {
          Id = 3,
          Title = "Gender Pay Gap in 2018",
          TypeId = 2,
          Url = "assets/pdf/Gender-Pay-Gap.pdf",
          Preview = "assets/pdf/preview/Gender-Pay-Gap-01.jpg",
          Description =
            "How Large Is It, How Much It Grows As Workers Climb The Corporate Ladder, and How Career Disruptions Perpetuate The Gender Pay Gap"
        }
      };

      if (string.IsNullOrEmpty(search))
      {
        search = string.Empty;
      }

      if (typeFilter != 0)
      {
        var results = await Task.Run(() => libraryItems.Where(x => x.TypeId == typeFilter && x.Title.ToLower().Contains(search.ToLower())));
        return results;
      }
      else
      {
        var results = await Task.Run(() => libraryItems.Where(x => x.Title.ToLower().Contains(search.ToLower())));
        return results;
      }
    }
  }
}
