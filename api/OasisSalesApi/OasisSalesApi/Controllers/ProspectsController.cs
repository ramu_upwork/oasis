using OasisSalesApi.BL;
using OasisSalesApi.ViewModel;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebSupergoo.ABCpdf10;

namespace OasisSalesApi.Controllers
{
  public class ProspectsController : BaseController
  {
    private readonly BL.IProspectService _service;

    protected override ServiceBase Service => (ServiceBase)this._service;

    public ProspectsController()
             : this(new ProspectService())
    {

    }

    public ProspectsController(IProspectService service)
    {
      this._service = service;
    }

    [HttpGet]
    public async Task<object> GetProspects(int statusFilter = 0, string search = "")
    {
      if (string.IsNullOrEmpty(search))
      {
        search = string.Empty;
      }
      return new { Prospects = await _service.GetProspectList(statusFilter, search, this.LoggedInUser.Email) };
    }

    [HttpGet]
    public async Task<object> GetProspect(Guid identifier)
    {
      return new { Prospect = await _service.GetProspect(identifier) };
    }

    [HttpGet]
    public async Task<object> GetProspectDocuments(Guid identifier)
    {
      return new { Documents = await _service.GetProspectDocuments(identifier) };
    }

    [HttpGet]
    public object GenerateSummary(Guid identifier)
    {
      return this._service.GenerateSummary(identifier, this.LoggedInUser.Name);
    }

    [HttpGet]
    public async Task<object> GetHealthSummary(Guid identifier)
    {
      return new { HealthSummary = await this._service.GetProspectHealthSummary(identifier) };
    }

    [HttpGet]
    public HttpResponseMessage DownLoadSummary(Guid identifier)
    {
      try
      {
        var summaryDoc = new Doc();
        summaryDoc.Read(HttpContext.Current.Server.MapPath("~/ProspectDocuments/SummaryPdf/" + identifier + ".pdf"));
        var response = new HttpResponseMessage(HttpStatusCode.OK);
        Stream stream = new MemoryStream(summaryDoc.GetData());
        response.Content = new StreamContent(stream);
        response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
        response.Content.Headers.ContentDisposition =
          new ContentDispositionHeaderValue("inline") { FileName = $"SummaryReport_{identifier}.pdf" };
        return response;
      }
      catch (Exception ex)
      {
        return this.Request.CreateResponse(HttpStatusCode.OK, new { Message = "File Not Found" });
      }
    }

    [HttpGet]
    public HttpResponseMessage ViewHealthSummary(Guid identifier)
    {
      try
      {

        var html = this._service.ViewHealthSummary(identifier);
        var pdf = ConvertHtmlToPdf(html);
        var response = new HttpResponseMessage(HttpStatusCode.OK);
        Stream stream = new MemoryStream(pdf);
        response.Content = new StreamContent(stream);
        response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
        response.Content.Headers.ContentDisposition =
          new ContentDispositionHeaderValue("inline") { FileName = $"Report_Html.pdf" };
        return response;
      }
      catch (Exception ex)
      {
        return this.Request.CreateResponse(HttpStatusCode.OK, new { Message = "File Not Found" });
      }
    }

    [HttpPost]
    public async Task<object> SendZipFile(SendZipFileVM zipFileMail)
    {
      return await _service.SendZipFile(zipFileMail);
    }


  }
}
