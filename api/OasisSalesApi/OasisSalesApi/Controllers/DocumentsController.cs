using OasisSalesApi.BL;
using OasisSalesApi.ViewModel;
using System.Threading.Tasks;
using System.Web.Http;

namespace OasisSalesApi.Controllers
{
  public class DocumentsController : BaseController
  {
    private readonly BL.IDocumentService _service;

    protected override ServiceBase Service => (ServiceBase)this._service;

    public DocumentsController()
            : this(new DocumentService())
    {

    }

    public DocumentsController(IDocumentService service)
    {
      this._service = service;
    }


    [HttpGet]
    public async Task<object> GetDocumentTypes()
    {
      return new { DocumentTypes = await _service.GetDocumentTypes() };
    }

    [HttpGet]
    public async Task<object> GetDocument(int id)
    {
      return new { Id = id, Document = await _service.GetDocument(id) };
    }

    [HttpPost]
    public object UploadDocument(DocumentUploadVM payload)
    {
      return new { Result = _service.UploadDocument(payload) };
    }

  }
}
