using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OasisSalesApi.BL;
using OasisSalesApi.ViewModel;

namespace OasisSalesApi.Controllers
{
  public class AuthenticationController : BaseController
  {
    private readonly BL.IAuthenticationService _service;

    protected override ServiceBase Service => (ServiceBase)this._service;

    public AuthenticationController()
      : this(new AuthenticationService())
    {

    }

    public AuthenticationController(IAuthenticationService service)
    {
      this._service = service;
    }

    [HttpPost]
    public IHttpActionResult ValidateUser(CredentialsVM credentilas)
    {
      return Ok(_service.ValidateUser(credentilas));
    }

    [HttpGet]
    public IHttpActionResult GetUser()
    {
      return Ok(this.LoggedInUser?.Name);
    }
  }
}
