using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OasisSalesApi.Utility
{
  public static class OasisUtility
  {
    public static FileInfo GetFileExtension(string base64String)
    {
      var data = base64String.Substring(0, 5);

      switch (data.ToUpper())
      {
        case "IVBOR":
          //return "png";
          return new FileInfo {Name= Guid.NewGuid().ToString()+".png",MediaTypeHeaderValue = "image/jpeg" };
        case "/9J/4":
          //return "jpg";
          return new FileInfo { Name = Guid.NewGuid().ToString() + ".jpg", MediaTypeHeaderValue = "image/jpeg" };
        case "AAAAF":
          //return "mp4";
          return new FileInfo { Name = Guid.NewGuid().ToString() + ".mp4", MediaTypeHeaderValue = "video/mp4" };
        case "JVBER":
          //return "pdf";
          return new FileInfo { Name = Guid.NewGuid().ToString() + ".pdf", MediaTypeHeaderValue = "application/pdf" };
        case "AAABA":
          //return "ico";
          return new FileInfo { Name = Guid.NewGuid().ToString() + ".ico", MediaTypeHeaderValue = "image/x-icon" };
        //case "UMFYI":
        //  return "rar";
        //case "E1XYD":
        //  return "rtf";
        //case "U1PKC":
        //  return "txt";
        //case "MQOWM":
        //case "77U/M":
        //  return "srt";
        default:
          return new FileInfo { Name = Guid.NewGuid().ToString() + ".jpg", MediaTypeHeaderValue = "image/jpeg" };
      }
    }

    public static byte[]  ParseBase64ToArray(string base64String)
    {
      string[] words = base64String.Split(',');
      if (words.Length == 2)
      {
        return Convert.FromBase64String(words[1]);
      }
      else
      {
        return null;
      }
    }
      

public class FileInfo
    {
      public string Name { get; set; }
      public string MediaTypeHeaderValue { get; set; }
    }
  }
}
