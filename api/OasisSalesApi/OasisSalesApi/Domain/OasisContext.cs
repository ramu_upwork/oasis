using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace OasisSalesApi.Domain
{
  public class OasisContext : DbContext
  {

    public OasisContext() : base("DefaultConnection")
    {

    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      Database.SetInitializer<OasisContext>(null);
      base.OnModelCreating(modelBuilder);
    }

    public DbSet<User> Users { get; set; }
    public DbSet<QuestionVertical> QuestionVerticals { get; set; }
    public DbSet<VerticalSection> VerticalSections { get; set; }
    public DbSet<Question> Questions { get; set; }
    public DbSet<Answer> Answers { get; set; }
    public DbSet<Prospect> Prospects { get; set; }
    public DbSet<ProspectStatus> ProspectStatus { get; set; }
    public DbSet<Company> Companies { get; set; }
    public DbSet<DocumentType> DocumentTypes { get; set; }
    public DbSet<DocumentSubType> DocumentSubTypes { get; set; }
    public DbSet<Document> Documents { get; set; }
    public DbSet<QuestionaireResultSet> QuestionaireResultSets { get; set; }
    public DbSet<QuestionaireResult> QuestionaireResult { get; set; }
    public DbSet<QuestionaireResultPdf> QuestionaireResultPdf { get; set; }
    public DbSet<Research> Research { get; set; }
    public DbSet<ResearchDocument> ResearchDocuments { get; set; }
    public DbSet<ProspectSummaryTemplate> ProspectSummaryTemplates { get; set; }

  }

  public static class Extensions
  {
    public static IQueryable<Prospect> ProspectDetails(this OasisContext context, Expression<Func<Prospect, bool>> predicate = null)
    {
      if (predicate != null)
      {
        return context.Prospects
         .Include("Status")
         .Where(predicate);
      }
      else
      {
        return context.Prospects
        .Include("Status");
      }

    }
  }
}
