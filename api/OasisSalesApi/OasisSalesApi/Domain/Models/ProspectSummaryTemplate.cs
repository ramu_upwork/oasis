using System.ComponentModel.DataAnnotations;

namespace OasisSalesApi.Domain
{
  public class ProspectSummaryTemplate
  {
    public int Id { get; set; }

    [StringLength(int.MaxValue)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Template { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Description { get; set; }

    public int PageNumber { get; set; }

    public bool IsActive { get; set; }
  }
}
