using System.ComponentModel.DataAnnotations.Schema;

namespace OasisSalesApi.Domain
{
  public class QuestionaireResultSet
  {
    public int Id { get; set; }

    [ForeignKey("QuestionaireResult")]
    public int QuestionaireResultId { get; set; }

    [ForeignKey("VerticalSection")]
    public int SectionId { get; set; }

    [ForeignKey("Question")]
    public int QuestionId { get; set; }

    [ForeignKey("Answer")]
    public int AnswerId { get; set; }


    public VerticalSection VerticalSection { get; set; }

    public QuestionaireResult QuestionaireResult { get; set; }

    public Question Question { get; set; }

    public Answer Answer { get; set; }

  }
}
