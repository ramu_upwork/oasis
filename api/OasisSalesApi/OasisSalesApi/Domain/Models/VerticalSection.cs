using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OasisSalesApi.Domain
{
  public class VerticalSection
  {
    public int Id { get; set; }

    public int QuestionVerticalId { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Name { get; set; }

    [StringLength(int.MaxValue)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Recommendations { get; set; }

    public int Rank { get; set; }

    public bool IsActive { get; set; }

    public virtual ICollection<Question> Questions { get; set; }
  }
}
