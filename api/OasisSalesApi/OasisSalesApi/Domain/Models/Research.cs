using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OasisSalesApi.Domain
{
  public class Research
  {
    public int Id { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Name { get; set; }

    [StringLength(500)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Description { get; set; }

    [StringLength(500)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Url { get; set; }

    [StringLength(int.MaxValue)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string CoverPageTemplate { get; set; }

    public int Rank { get; set; }

    public virtual ICollection<ResearchDocument> Documents { get; set; }

  }

  
}
