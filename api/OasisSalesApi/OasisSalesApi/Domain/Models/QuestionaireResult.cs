using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OasisSalesApi.Domain
{
  public class QuestionaireResult
  {
    public int Id { get; set; }

    public Guid ProspectId { get; set; }

    [ForeignKey("QuestionVertical")]
    public int QuestionVerticalId { get; set; }

    [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
    public DateTime CreatedDate { get; set; }

    public QuestionVertical QuestionVertical { get; set; }

    public virtual ICollection<QuestionaireResultSet> ResultSets { get; set; }

    public virtual ICollection<QuestionaireResultPdf> PdfContent { get; set; }
  }
}
