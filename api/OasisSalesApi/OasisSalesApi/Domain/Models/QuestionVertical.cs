using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OasisSalesApi.Domain
{
  public class QuestionVertical
  {
    public int Id { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Name { get; set; }

    public int Rank { get; set; }

    public bool IsActive { get; set; }

    public virtual ICollection<VerticalSection> Sections { get; set; }

  }
}
