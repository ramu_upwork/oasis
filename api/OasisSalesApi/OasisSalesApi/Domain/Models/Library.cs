namespace OasisSalesApi.Domain
{
  public class Library
  {
    public int Id { get; set; }

    public int TypeId { get; set; }

    public string Title { get; set; }

    public string Url { get; set; }

    public string Preview { get; set; }

    public string Description { get; set; }

  }
}
