using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OasisSalesApi.Domain
{
  public class Question
  {
    public int Id { get; set; }

    public int VerticalSectionId { get; set; }

    [StringLength(int.MaxValue)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Copy { get; set; }

    public int Rank { get; set; }

    public bool IsActive { get; set; }

    public virtual ICollection<Answer> Answers { get; set; }


  }
}
