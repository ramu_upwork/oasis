using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OasisSalesApi.Domain
{
  public class DocumentSubType
  {
    public int Id { get; set; }

    [ForeignKey("Type")]
    [Required]
    public int TypeId { get; set; }

    [ForeignKey("Prospect")]
    public int? ProspectId { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Name { get; set; }

    [StringLength(500)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Description { get; set; }

    public int Rank { get; set; }

    public bool IsRequired { get; set; }

    public bool IsActive { get; set; }

    public DocumentType Type { get; set; }

    public virtual ICollection<Document> Documents { get; set; }

    public Prospect Prospect { get; set; }
  }
}
