using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OasisSalesApi.Domain
{
  public class Document
  {
    public int Id { get; set; }

    [ForeignKey("Prospect")]
    [Required]
    public int ProspectId { get; set; }

    [ForeignKey("Type")]
    [Required]
    public int TypeId { get; set; }

    public int Rank { get; set; }

    [StringLength(int.MaxValue)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string FileContent { get; set; }

    public Guid UniqueId { get; set; }

    public bool IsActive { get; set; }

    public DocumentSubType Type { get; set; }

    public Prospect Prospect { get; set; }

  }
}
