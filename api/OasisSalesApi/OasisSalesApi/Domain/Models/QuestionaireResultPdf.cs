using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OasisSalesApi.Domain
{
  public class QuestionaireResultPdf
  {
    public int Id { get; set; }

    [ForeignKey("QuestionaireResult")]
    public int QuestionaireResultId { get; set; }

    [StringLength(int.MaxValue)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Page { get; set; }


    public int PageNumber { get; set; }

    public QuestionaireResult QuestionaireResult { get; set; }

  }
}
