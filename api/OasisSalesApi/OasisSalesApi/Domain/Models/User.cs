using System;

namespace OasisSalesApi.Domain
{
  public class User
  {

    public int Id { get; set; }

    public int OwnerUID { get; set; }

    public string ADandCSUID { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Email { get; set; }

    public string Password { get; set; }

    public bool IsActive { get; set; }

    public DateTime CreatedDate { get; set; }

  }
}
