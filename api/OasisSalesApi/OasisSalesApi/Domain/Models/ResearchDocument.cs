using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OasisSalesApi.Domain
{
  public class ResearchDocument
  {
    public int Id { get; set; }

    [ForeignKey("Research")]
    [Required]
    public int ResearchId { get; set; }

    [ForeignKey("Prospect")]
    [Required]
    public int ProspectId { get; set; }

    [StringLength(int.MaxValue)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string FileContent { get; set; }

    [StringLength(100)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string FileType { get; set; }

    public Guid UniqueId { get; set; }

    [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
    public DateTime CreatedDate { get; set; }

    public Prospect Prospect { get; set; }
    public Research Research { get; set; }

  }
}
