using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OasisSalesApi.Domain
{
  public class DocumentType
  {
    public int Id { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Name { get; set; }

    [StringLength(500)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Description { get; set; }

    public int Rank { get; set; }

    public bool IsActive { get; set; }

    public virtual ICollection<DocumentSubType> SubTypes { get; set; }
  }
}
