using System.ComponentModel.DataAnnotations;

namespace OasisSalesApi.Domain
{
  public class Company
  {
    public int Id { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Name { get; set; }

    [StringLength(500)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Description { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Type { get; set; }
  }
}
