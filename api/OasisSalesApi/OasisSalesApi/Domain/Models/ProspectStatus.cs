using System.ComponentModel.DataAnnotations;

namespace OasisSalesApi.Domain
{
  public class ProspectStatus
  {
    public int Id { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Name { get; set; }
  }
}
