using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OasisSalesApi.Domain
{

  public class Prospect
  {
    public int Id { get; set; }

    public int OrgUID { get; set; }

    [ForeignKey("Status")]
    public int StatusId { get; set; }

    public int CMUID { get; set; }

    public int ProjectUID { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Name { get; set; }

    [StringLength(250)]
    public string DBA { get; set; }

    [StringLength(500)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Address1 { get; set; }

    [StringLength(500)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Address2 { get; set; }

    [StringLength(250)]
    public string City { get; set; }

    [StringLength(250)]
    public string State { get; set; }

    [StringLength(250)]
    public string PostalCode { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Phone { get; set; }

    [StringLength(250)]
    public string AssociateEmail { get; set; }

    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Website { get; set; }

    public int EmployeeCount { get; set; }

    public string CurrentPEO { get; set; }

    public string BenefitsCarrier { get; set; }

    public string WCCarrier { get; set; }

    [Required]
    public Guid UniqueId { get; set; }
    
    [StringLength(250)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Fax { get; set; }

    [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
    public DateTime CreatedDate { get; set; }

    public ProspectStatus Status { get; set; }

    public virtual ICollection<Document> Documents { get; set; }

    public bool IsSummaryCreated { get; set; }

    [StringLength(int.MaxValue)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string SummaryFile { get; set; }

    [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
    public DateTime SummaryCreatedDate { get; set; }

  }
}
