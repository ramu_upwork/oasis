using System.ComponentModel.DataAnnotations;

namespace OasisSalesApi.Domain
{
  public class Answer
  {
    public int Id { get; set; }

    public int QuestionId { get; set; }

    [StringLength(int.MaxValue)]
    [DisplayFormat(NullDisplayText = "Empty")]
    public string Copy { get; set; }

    public int Rank { get; set; }

    public bool IsActive { get; set; }

  }
}
