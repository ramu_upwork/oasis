using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace OasisSalesApi.Domain.Token
{
  public class Token
  {
    public Token(int id, string name, string role,string email)
    {
      Id = id;
      Name = name;
      Role = role;
      Email = email;

    }

    public int Id { get; set; }
    public string Name { get; set; }
    public string Role { get; set; }

    public string Email { get; set; }

    private const string PasswordHash = "P@@Sw0rd";
    private const string SaltKey = "S@LT&KEY";
    private const string ViKey = "@1B2c3D4e5F6g7H8";

    public string Encrypt()
    {

      byte[] plainTextBytes = Encoding.UTF8.GetBytes(this.ToString());
      byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
      var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
      var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(ViKey));

      byte[] cipherTextBytes;

      using (var memoryStream = new MemoryStream())
      {
        using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
        {
          cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
          cryptoStream.FlushFinalBlock();
          cipherTextBytes = memoryStream.ToArray();
          cryptoStream.Close();
        }
        memoryStream.Close();
      }
      return Convert.ToBase64String(cipherTextBytes);
    }

    public override string ToString()
    {
      return $"Id={ this.Id}$Name={this.Name}$Role={this.Role}$Email={ this.Email}";
    }

    public static Token Decrypt(string encryptedToken)
    {
      try
      {
        byte[] cipherTextBytes = Convert.FromBase64String(HttpUtility.UrlDecode(encryptedToken)?.Replace(' ', '+'));
        byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
        var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

        var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(ViKey));
        var memoryStream = new MemoryStream(cipherTextBytes);
        var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
        var plainTextBytes = new byte[cipherTextBytes.Length];

        int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
        memoryStream.Close();
        cryptoStream.Close();
        string decrypted = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount)
            .TrimEnd("\0".ToCharArray());
        ////Splitting it to dictionary
        Dictionary<string, string> dictionary = decrypted.ToDictionary();
        return new Token(Convert.ToInt32(dictionary["Id"]),
            dictionary["Name"], dictionary["Role"], dictionary["Email"]);
      }
      catch (Exception ex)
      {
        return null;
      }

    }
  }
  public static class Extensions
  {
    public static Dictionary<string, string> ToDictionary(this string keyValue)
    {
      return keyValue.Split(new[] { '$' }, StringSplitOptions.RemoveEmptyEntries)
        .Select(part => part.Split('='))
        .ToDictionary(split => split[0], split => split[1]);
    }
  }
}
