namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class relate_Prospect_ProspectStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prospects", "StatusId", c => c.Int( nullable: false, defaultValue:1));
            CreateIndex("dbo.Prospects", "StatusId");
            AddForeignKey("dbo.Prospects", "StatusId", "dbo.ProspectStatus", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Prospects", "StatusId", "dbo.ProspectStatus");
            DropIndex("dbo.Prospects", new[] { "StatusId" });
            DropColumn("dbo.Prospects", "StatusId");
        }
    }
}
