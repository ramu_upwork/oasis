namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionId = c.Int(nullable: false),
                        Copy = c.String(),
                        Rank = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VerticalSectionId = c.Int(nullable: false),
                        Copy = c.String(),
                        Rank = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VerticalSections", t => t.VerticalSectionId, cascadeDelete: true)
                .Index(t => t.VerticalSectionId);
            
            CreateTable(
                "dbo.QuestionVerticals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Rank = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VerticalSections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionVerticalId = c.Int(nullable: false),
                        Name = c.String(maxLength: 250),
                        Rank = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionVerticals", t => t.QuestionVerticalId, cascadeDelete: true)
                .Index(t => t.QuestionVerticalId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VerticalSections", "QuestionVerticalId", "dbo.QuestionVerticals");
            DropForeignKey("dbo.Questions", "VerticalSectionId", "dbo.VerticalSections");
            DropForeignKey("dbo.Answers", "QuestionId", "dbo.Questions");
            DropIndex("dbo.VerticalSections", new[] { "QuestionVerticalId" });
            DropIndex("dbo.Questions", new[] { "VerticalSectionId" });
            DropIndex("dbo.Answers", new[] { "QuestionId" });
            DropTable("dbo.VerticalSections");
            DropTable("dbo.QuestionVerticals");
            DropTable("dbo.Questions");
            DropTable("dbo.Answers");
        }
    }
}
