namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_table_ProspectSummaryTemplates_add_column_CoverPageTemplate_inResearch_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProspectSummaryTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Template = c.String(),
                        Description = c.String(maxLength: 250),
                        PageNumber = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Researches", "CoverPageTemplate", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Researches", "CoverPageTemplate");
            DropTable("dbo.ProspectSummaryTemplates");
        }
    }
}
