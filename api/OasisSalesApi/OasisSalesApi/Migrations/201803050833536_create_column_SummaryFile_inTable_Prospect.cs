namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_column_SummaryFile_inTable_Prospect : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prospects", "SummaryFile", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prospects", "SummaryFile");
        }
    }
}
