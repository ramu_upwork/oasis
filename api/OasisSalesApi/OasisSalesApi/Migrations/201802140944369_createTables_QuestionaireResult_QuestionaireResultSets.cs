namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createTables_QuestionaireResult_QuestionaireResultSets : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuestionaireResults",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProspectId = c.Guid(nullable: false),
                        QuestionVerticalId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionVerticals", t => t.QuestionVerticalId, cascadeDelete: true)
                .Index(t => t.QuestionVerticalId);
            
            CreateTable(
                "dbo.QuestionaireResultSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionaireResultId = c.Int(nullable: false),
                        SectionId = c.Int(nullable: false),
                        QuestionId = c.Int(nullable: false),
                        AnswerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Answers", t => t.AnswerId, cascadeDelete: false)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: false)
                .ForeignKey("dbo.QuestionaireResults", t => t.QuestionaireResultId, cascadeDelete: false)
                .ForeignKey("dbo.VerticalSections", t => t.SectionId, cascadeDelete: false)
                .Index(t => t.QuestionaireResultId)
                .Index(t => t.SectionId)
                .Index(t => t.QuestionId)
                .Index(t => t.AnswerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuestionaireResultSets", "SectionId", "dbo.VerticalSections");
            DropForeignKey("dbo.QuestionaireResultSets", "QuestionaireResultId", "dbo.QuestionaireResults");
            DropForeignKey("dbo.QuestionaireResultSets", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.QuestionaireResultSets", "AnswerId", "dbo.Answers");
            DropForeignKey("dbo.QuestionaireResults", "QuestionVerticalId", "dbo.QuestionVerticals");
            DropIndex("dbo.QuestionaireResultSets", new[] { "AnswerId" });
            DropIndex("dbo.QuestionaireResultSets", new[] { "QuestionId" });
            DropIndex("dbo.QuestionaireResultSets", new[] { "SectionId" });
            DropIndex("dbo.QuestionaireResultSets", new[] { "QuestionaireResultId" });
            DropIndex("dbo.QuestionaireResults", new[] { "QuestionVerticalId" });
            DropTable("dbo.QuestionaireResultSets");
            DropTable("dbo.QuestionaireResults");
        }
    }
}
