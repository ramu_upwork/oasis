namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class errorCorrcetionAddedAssociateColumnInWrongTable_AddSameColumnToPrpspectTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prospects", "Associate", c => c.String(nullable: false, defaultValue: "john@sgcagency.com"));
            DropColumn("dbo.ProspectSummaryTemplates", "Associate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProspectSummaryTemplates", "Associate", c => c.String());
            DropColumn("dbo.Prospects", "Associate");
        }
    }
}
