namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class included_columns_IsSummaryCreated_SummaryCreatedDate_inTable_Prospect : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prospects", "IsSummaryCreated", c => c.Boolean(nullable: false));
            AddColumn("dbo.Prospects", "SummaryCreatedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prospects", "SummaryCreatedDate");
            DropColumn("dbo.Prospects", "IsSummaryCreated");
        }
    }
}
