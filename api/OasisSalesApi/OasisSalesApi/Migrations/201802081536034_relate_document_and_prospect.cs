namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class relate_document_and_prospect : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "ProspectId", c => c.Int(nullable: false));
            CreateIndex("dbo.Documents", "ProspectId");
            AddForeignKey("dbo.Documents", "ProspectId", "dbo.Prospects", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Documents", "ProspectId", "dbo.Prospects");
            DropIndex("dbo.Documents", new[] { "ProspectId" });
            DropColumn("dbo.Documents", "ProspectId");
        }
    }
}
