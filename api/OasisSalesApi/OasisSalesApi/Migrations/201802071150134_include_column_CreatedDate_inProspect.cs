namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class include_column_CreatedDate_inProspect : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prospects", "CreatedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prospects", "CreatedDate");
        }
    }
}
