namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class prospects_users_table_restructure : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Prospects", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Prospects", new[] { "CompanyId" });
            AddColumn("dbo.Prospects", "OrgUID", c => c.Int(nullable: false));
            AddColumn("dbo.Prospects", "CMUID", c => c.Int(nullable: false));
            AddColumn("dbo.Prospects", "ProjectUID", c => c.Int(nullable: false));
            AddColumn("dbo.Prospects", "DBA", c => c.String(maxLength: 250));
            AddColumn("dbo.Prospects", "Address1", c => c.String(maxLength: 500));
            AddColumn("dbo.Prospects", "Address2", c => c.String(maxLength: 500));
            AddColumn("dbo.Prospects", "City", c => c.String(maxLength: 250));
            AddColumn("dbo.Prospects", "State", c => c.String(maxLength: 250));
            AddColumn("dbo.Prospects", "PostalCode", c => c.String(maxLength: 250));
            AddColumn("dbo.Prospects", "AssociateEmail", c => c.String(maxLength: 250));
            AddColumn("dbo.Prospects", "EmployeeCount", c => c.Int(nullable: false));
            AddColumn("dbo.Prospects", "CurrentPEO", c => c.String());
            AddColumn("dbo.Prospects", "BenefitsCarrier", c => c.String());
            AddColumn("dbo.Prospects", "WCCarrier", c => c.String());
            AddColumn("dbo.Users", "OwnerUID", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "ADandCSUID", c => c.String());
            DropColumn("dbo.Prospects", "Associate");
            DropColumn("dbo.Prospects", "CompanyId");
            DropColumn("dbo.Prospects", "EECount");
            DropColumn("dbo.Prospects", "Location");
            DropColumn("dbo.Prospects", "Email");
            DropColumn("dbo.Prospects", "Address");
            DropColumn("dbo.Prospects", "DisQualifiedReason");
            DropColumn("dbo.Prospects", "DisQualifiedDate");
            DropColumn("dbo.Prospects", "NextSteps");
            DropColumn("dbo.Prospects", "PastActivity");
            DropColumn("dbo.Prospects", "ReconnectDate");
            DropColumn("dbo.Prospects", "InterestLevel");
            DropColumn("dbo.Prospects", "Interests");
            DropColumn("dbo.Prospects", "InHouseService");
            DropColumn("dbo.Prospects", "PEOVendor");
            DropColumn("dbo.Prospects", "PayrollVendor");
            DropColumn("dbo.Prospects", "InsuranceCarrier");
            DropColumn("dbo.Prospects", "InsuranceRenewalDate");
            DropColumn("dbo.Prospects", "SalesNotes");
            DropColumn("dbo.Prospects", "NAIC_SIC");
            DropColumn("dbo.Prospects", "AddressMap");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Prospects", "AddressMap", c => c.String(maxLength: 500));
            AddColumn("dbo.Prospects", "NAIC_SIC", c => c.String(maxLength: 500));
            AddColumn("dbo.Prospects", "SalesNotes", c => c.String(maxLength: 500));
            AddColumn("dbo.Prospects", "InsuranceRenewalDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Prospects", "InsuranceCarrier", c => c.String(maxLength: 250));
            AddColumn("dbo.Prospects", "PayrollVendor", c => c.String(maxLength: 250));
            AddColumn("dbo.Prospects", "PEOVendor", c => c.String(maxLength: 500));
            AddColumn("dbo.Prospects", "InHouseService", c => c.String(maxLength: 500));
            AddColumn("dbo.Prospects", "Interests", c => c.String(maxLength: 500));
            AddColumn("dbo.Prospects", "InterestLevel", c => c.String(maxLength: 250));
            AddColumn("dbo.Prospects", "ReconnectDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Prospects", "PastActivity", c => c.String(maxLength: 500));
            AddColumn("dbo.Prospects", "NextSteps", c => c.String(maxLength: 500));
            AddColumn("dbo.Prospects", "DisQualifiedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Prospects", "DisQualifiedReason", c => c.String());
            AddColumn("dbo.Prospects", "Address", c => c.String(maxLength: 500));
            AddColumn("dbo.Prospects", "Email", c => c.String(maxLength: 250));
            AddColumn("dbo.Prospects", "Location", c => c.String(maxLength: 250));
            AddColumn("dbo.Prospects", "EECount", c => c.Int(nullable: false));
            AddColumn("dbo.Prospects", "CompanyId", c => c.Int(nullable: false));
            AddColumn("dbo.Prospects", "Associate", c => c.String());
            DropColumn("dbo.Users", "ADandCSUID");
            DropColumn("dbo.Users", "OwnerUID");
            DropColumn("dbo.Prospects", "WCCarrier");
            DropColumn("dbo.Prospects", "BenefitsCarrier");
            DropColumn("dbo.Prospects", "CurrentPEO");
            DropColumn("dbo.Prospects", "EmployeeCount");
            DropColumn("dbo.Prospects", "AssociateEmail");
            DropColumn("dbo.Prospects", "PostalCode");
            DropColumn("dbo.Prospects", "State");
            DropColumn("dbo.Prospects", "City");
            DropColumn("dbo.Prospects", "Address2");
            DropColumn("dbo.Prospects", "Address1");
            DropColumn("dbo.Prospects", "DBA");
            DropColumn("dbo.Prospects", "ProjectUID");
            DropColumn("dbo.Prospects", "CMUID");
            DropColumn("dbo.Prospects", "OrgUID");
            CreateIndex("dbo.Prospects", "CompanyId");
            AddForeignKey("dbo.Prospects", "CompanyId", "dbo.Companies", "Id", cascadeDelete: true);
        }
    }
}
