namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createTable_QuestionaireResultPdf : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuestionaireResultPdfs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionaireResultId = c.Int(nullable: false),
                        Page = c.String(),
                        PageNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionaireResults", t => t.QuestionaireResultId, cascadeDelete: true)
                .Index(t => t.QuestionaireResultId);
            
            DropColumn("dbo.QuestionaireResults", "Pdf");
        }
        
        public override void Down()
        {
            AddColumn("dbo.QuestionaireResults", "Pdf", c => c.String());
            DropForeignKey("dbo.QuestionaireResultPdfs", "QuestionaireResultId", "dbo.QuestionaireResults");
            DropIndex("dbo.QuestionaireResultPdfs", new[] { "QuestionaireResultId" });
            DropTable("dbo.QuestionaireResultPdfs");
        }
    }
}
