namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class column_updates_for_documentTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "FileContent", c => c.String());
            DropColumn("dbo.Documents", "Name");
            DropColumn("dbo.Documents", "FileName");
            DropColumn("dbo.Documents", "Description");
            DropColumn("dbo.Documents", "FilePath");
            DropColumn("dbo.Documents", "Extension");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Documents", "Extension", c => c.String(maxLength: 50));
            AddColumn("dbo.Documents", "FilePath", c => c.String());
            AddColumn("dbo.Documents", "Description", c => c.String(maxLength: 500));
            AddColumn("dbo.Documents", "FileName", c => c.String(maxLength: 250));
            AddColumn("dbo.Documents", "Name", c => c.String(maxLength: 250));
            DropColumn("dbo.Documents", "FileContent");
        }
    }
}
