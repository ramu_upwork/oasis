namespace OasisSalesApi.Migrations
{
  using OasisSalesApi.Domain;
  using System;
  using System.Data.Entity;
  using System.Data.Entity.Migrations;
  using System.Linq;

  internal sealed class Configuration : DbMigrationsConfiguration<OasisSalesApi.Domain.OasisContext>
  {
    public Configuration()
    {
      AutomaticMigrationsEnabled = false;
    }

    protected override void Seed(OasisSalesApi.Domain.OasisContext context)
    {
      context.ProspectStatus.AddOrUpdate(x => x.Id,
       new OasisSalesApi.Domain.ProspectStatus() { Id = 1, Name = "Pending" },
       new OasisSalesApi.Domain.ProspectStatus() { Id = 2, Name = "New" },
       new OasisSalesApi.Domain.ProspectStatus() { Id = 3, Name = "Open" }
      );

      context.Research.AddOrUpdate(x => x.Id,
              new OasisSalesApi.Domain.Research()
              {
                Id = 1,
                Name = "SALARY",
                Description = "Payscale links individual businesses to the largest salary profile databases in the world.",
                Rank = 1,
                Url = "http://payscale.com"
              },
              new OasisSalesApi.Domain.Research()
              {
                Id = 2,
                Name = "Benefits",
                Description = "Allows you to analyze pay competitiveness and automate market pricing all in a single, cloud based solution.",
                Rank = 2,
                Url = "http://salary.com"
              },
              new OasisSalesApi.Domain.Research()
              {
                Id = 3,
                Name = "401K",
                Description = "Internal Info",
                Rank = 3,
                Url = "https://www.slavic401k.com/planhub.jsp"
              },
              new OasisSalesApi.Domain.Research()
              {
                Id = 4,
                Name = "Website",
                Description = "Nibbler will give you a report scoring  your website 1 out of 10 for key areas, including accessibility, SEO, social media and technology.",
                Rank = 4,
                Url = "http://nibbler.silktide.com/en_US"
              }
             );

      context.Companies.AddOrUpdate(x => x.Id,
              new OasisSalesApi.Domain.Company() { Id = 1, Name = "Sg. Digital", Description = "JOHN FRANCHINA", Type = "Principal" }
             );
    }
  }
}
