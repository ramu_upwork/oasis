namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedColumnRecommendationsInTableVerticalSections : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VerticalSections", "Recommendations", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.VerticalSections", "Recommendations");
        }
    }
}
