namespace OasisSalesApi.Migrations
{
  using System;
  using System.Data.Entity.Migrations;

  public partial class updatedColumnTypesInUserTable : DbMigration
  {
    public override void Up()
    {
      AddColumn("dbo.Users", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
      AlterColumn("dbo.Users", "CreatedDate", c => c.DateTime(nullable: false, defaultValue: DateTime.Now));
      DropColumn("dbo.Users", "IsAtive");
    }

    public override void Down()
    {
      AddColumn("dbo.Users", "IsAtive", c => c.String());
      AlterColumn("dbo.Users", "CreatedDate", c => c.String());
      DropColumn("dbo.Users", "IsActive");
    }
  }
}
