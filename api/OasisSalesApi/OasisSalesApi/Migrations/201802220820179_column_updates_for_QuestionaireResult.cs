namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class column_updates_for_QuestionaireResult : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuestionaireResults", "Pdf", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuestionaireResults", "Pdf");
        }
    }
}
