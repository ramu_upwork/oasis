namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class include_column_UniqueId_inProspect : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prospects", "UniqueId", c => c.Guid(nullable: false,defaultValue: Guid.NewGuid()));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prospects", "UniqueId");
        }
    }
}
