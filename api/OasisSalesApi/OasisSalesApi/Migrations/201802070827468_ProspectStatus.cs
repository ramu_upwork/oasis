namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProspectStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProspectStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProspectStatus");
        }
    }
}
