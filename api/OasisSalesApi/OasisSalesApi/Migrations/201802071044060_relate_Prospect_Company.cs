namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class relate_Prospect_Company : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prospects", "CompanyId", c => c.Int(nullable: false,defaultValue:1));
            CreateIndex("dbo.Prospects", "CompanyId");
            AddForeignKey("dbo.Prospects", "CompanyId", "dbo.Companies", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Prospects", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Prospects", new[] { "CompanyId" });
            DropColumn("dbo.Prospects", "CompanyId");
        }
    }
}
