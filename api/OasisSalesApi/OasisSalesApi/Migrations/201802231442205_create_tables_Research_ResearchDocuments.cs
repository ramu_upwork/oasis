namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_tables_Research_ResearchDocuments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Researches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Description = c.String(maxLength: 500),
                        Url = c.String(maxLength: 500),
                        Rank = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ResearchDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ResearchId = c.Int(nullable: false),
                        ProspectId = c.Int(nullable: false),
                        FileContent = c.String(),
                        FileType = c.String(maxLength: 100),
                        UniqueId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Prospects", t => t.ProspectId, cascadeDelete: true)
                .ForeignKey("dbo.Researches", t => t.ResearchId, cascadeDelete: true)
                .Index(t => t.ResearchId)
                .Index(t => t.ProspectId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ResearchDocuments", "ResearchId", "dbo.Researches");
            DropForeignKey("dbo.ResearchDocuments", "ProspectId", "dbo.Prospects");
            DropIndex("dbo.ResearchDocuments", new[] { "ProspectId" });
            DropIndex("dbo.ResearchDocuments", new[] { "ResearchId" });
            DropTable("dbo.ResearchDocuments");
            DropTable("dbo.Researches");
        }
    }
}
