namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class relate_DocumentSubType_Prospect_include_column_IsRequired_in_DocumentSubType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DocumentSubTypes", "ProspectId", c => c.Int());
            AddColumn("dbo.DocumentSubTypes", "IsRequired", c => c.Boolean(nullable: false,defaultValue:true));
            CreateIndex("dbo.DocumentSubTypes", "ProspectId");
            AddForeignKey("dbo.DocumentSubTypes", "ProspectId", "dbo.Prospects", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DocumentSubTypes", "ProspectId", "dbo.Prospects");
            DropIndex("dbo.DocumentSubTypes", new[] { "ProspectId" });
            DropColumn("dbo.DocumentSubTypes", "IsRequired");
            DropColumn("dbo.DocumentSubTypes", "ProspectId");
        }
    }
}
