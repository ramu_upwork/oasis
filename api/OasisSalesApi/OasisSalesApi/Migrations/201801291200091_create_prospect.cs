namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_prospect : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Prospects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Website = c.String(maxLength: 250),
                        Phone = c.String(maxLength: 250),
                        Fax = c.String(maxLength: 250),
                        Email = c.String(maxLength: 250),
                        Address = c.String(maxLength: 500),
                        DisQualifiedReason = c.String(),
                        DisQualifiedDate = c.DateTime(nullable: false),
                        NextSteps = c.String(maxLength: 500),
                        PastActivity = c.String(maxLength: 500),
                        ReconnectDate = c.DateTime(nullable: false),
                        InterestLevel = c.String(maxLength: 250),
                        Interests = c.String(maxLength: 500),
                        InHouseService = c.String(maxLength: 500),
                        PEOVendor = c.String(maxLength: 500),
                        PayrollVendor = c.String(maxLength: 250),
                        InsuranceCarrier = c.String(maxLength: 250),
                        InsuranceRenewalDate = c.DateTime(nullable: false),
                        SalesNotes = c.String(maxLength: 500),
                        NAIC_SIC = c.String(maxLength: 500),
                        AddressMap = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Prospects");
        }
    }
}
