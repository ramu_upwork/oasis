namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class increased_columnSize_of_SummaryFile_in_Prospects_Table : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Prospects", "SummaryFile", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Prospects", "SummaryFile", c => c.String(maxLength: 500));
        }
    }
}
