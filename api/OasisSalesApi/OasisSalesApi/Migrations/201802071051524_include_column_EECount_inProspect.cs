namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class include_column_EECount_inProspect : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prospects", "EECount", c => c.Int(nullable: false,defaultValue:23));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prospects", "EECount");
        }
    }
}
