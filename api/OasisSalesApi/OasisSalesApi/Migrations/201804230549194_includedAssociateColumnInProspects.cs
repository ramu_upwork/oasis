namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class includedAssociateColumnInProspects : DbMigration
    {
        public override void Up()
        {
          AddColumn("dbo.ProspectSummaryTemplates", "Associate", c => c.String(nullable: false, defaultValue: "john@sgcagency.com"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProspectSummaryTemplates", "Associate");
        }
    }
}
