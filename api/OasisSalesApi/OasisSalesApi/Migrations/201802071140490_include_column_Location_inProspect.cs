namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class include_column_Location_inProspect : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prospects", "Location", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prospects", "Location");
        }
    }
}
