namespace OasisSalesApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_DocumentTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeId = c.Int(nullable: false),
                        Name = c.String(maxLength: 250),
                        FileName = c.String(maxLength: 250),
                        Description = c.String(maxLength: 500),
                        Rank = c.Int(nullable: false),
                        FilePath = c.String(),
                        Extension = c.String(maxLength: 50),
                        UniqueId = c.Guid(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DocumentSubTypes", t => t.TypeId, cascadeDelete: true)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.DocumentSubTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeId = c.Int(nullable: false),
                        Name = c.String(maxLength: 250),
                        Description = c.String(maxLength: 500),
                        Rank = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DocumentTypes", t => t.TypeId, cascadeDelete: true)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.DocumentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Description = c.String(maxLength: 500),
                        Rank = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Documents", "TypeId", "dbo.DocumentSubTypes");
            DropForeignKey("dbo.DocumentSubTypes", "TypeId", "dbo.DocumentTypes");
            DropIndex("dbo.DocumentSubTypes", new[] { "TypeId" });
            DropIndex("dbo.Documents", new[] { "TypeId" });
            DropTable("dbo.DocumentTypes");
            DropTable("dbo.DocumentSubTypes");
            DropTable("dbo.Documents");
        }
    }
}
